import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import StyledText from '../../components/StyledText';

const SplashImage = ({}) => (
  <View style={styles.container}>
    <StyledText style={styles.text}>
      Discover <StyledText style={styles.span}>EAT</StyledText>
    </StyledText>
    <Image style={styles.logo} resizeMode="contain" source={require('../splash.jpg')} />
  </View>
);

export default SplashImage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative'
  },
  logo: { flex: 1 },
  text: { position: 'absolute', top: '40%', left: 0, right: 0, textAlign: 'center', zIndex: 10000, color: '#fff', fontSize: 24 },
  span: { fontWeight: 'bold' }
});
