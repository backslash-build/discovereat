import { createStackNavigator } from 'react-navigation';
import SplashScreen from '../screens/SplashScreen';
import HomeScreen from '../screens/HomeScreen';
import DiscoverScreen from '../screens/DiscoverScreen';
import MealpackScreen from '../screens/MealpackScreen';
import CookbookScreen from '../screens/CookbookScreen';
import PreferencesScreen from '../screens/PreferencesScreen';
import LoginScreen from '../screens/LoginScreen';
import SignupScreen from '../screens/SignupScreen';
import RecipeScreen from '../screens/RecipeScreen';

export default (RootStackNavigator = createStackNavigator(
  {
    Discover: { screen: DiscoverScreen },
    Recipe: { screen: RecipeScreen },
    Home: { screen: HomeScreen },
    Splash: { screen: SplashScreen },
    Mealpack: { screen: MealpackScreen },
    Cookbook: { screen: CookbookScreen },
    Preferences: { screen: PreferencesScreen },
    Login: { screen: LoginScreen },
    Register: { screen: SignupScreen }
  },
  {
    navigationOptions: {
      headerMode: 'none',
      header: null,
      headerStyle: {
        backgroundColor: '#fff'
      },
      headerTitleStyle: {
        color: '#222'
      },
      headerBackTitleStyle: {
        color: '#222'
      },
      headerTintColor: '#222',
      gesturesEnabled: false
    }
  }
));
