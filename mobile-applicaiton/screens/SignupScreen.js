import React from 'react';
import { StyleSheet, View, ImageBackground, Dimensions } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import SingleLineInput from '../components/Forms/SingleLineInput';
import Button from '../components/Button';
import navigationOptions from '../navigation/staticNavigationOptions';
import ErrorMessage from '../components/Forms/ErrorMessage';
import StyledText from '../components/StyledText';
import { withAuth } from '../api/Auth';
import HyperText from '../components/HyperText/HyperText';

class LoginScreen extends React.Component {
  state = { email: '', password: '', firstName: '', submitting: false };
  componentDidUpdate = () => {
    if (this.props.auth.loggedIn) this.navigate('Home');
  };
  navigate = screen => {
    const navigateAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: screen })]
    });
    this.props.navigation.dispatch(navigateAction);
  };
  handleChange = ({ key, value }) => this.setState({ [key]: value });
  onPress = async () => {
    await this.setState({ submitting: true });
    const response = await this.props.auth.register(this.state.email, this.state.password);
    console.log('sign', response);
    await this.setState({ submitting: false });
  };
  onRegister = () => {
    this.navigate('Register');
  };
  render() {
    const { submitting } = this.state;
    const error = this.props.auth.error;
    return (
      <View style={styles.container}>
        <ImageBackground source={require('../assets/splash.jpg')} style={{ width: '100%', height: '100%' }}>
          <View style={styles.fullScreen}>
            <StyledText variant="h1" style={styles.text}>
              Sign Up
            </StyledText>
            <View style={styles.inputWrapper}>
              <SingleLineInput style={styles.input} onChange={value => this.handleChange({ value, key: 'firstName' })} label="First Name" clearIcon />
              <SingleLineInput style={styles.input} onChange={value => this.handleChange({ value, key: 'email' })} label="Email" clearIcon />
              <SingleLineInput style={styles.input} onChange={value => this.handleChange({ value, key: 'password' })} label="Password" secureTextEntry clearIcon />
            </View>
            <Button title="Sign up" submitting={submitting} onPress={this.onPress} />
            <StyledText variant="p2" style={styles.white}>
              Already have an account?
            </StyledText>
            <HyperText variant="p2" style={{ text: { color: '#ff423d' } }} screen="Login" navigation={this.props.navigation}>
              Log in
            </HyperText>
            {!!error && <ErrorMessage message={error} />}
          </View>
        </ImageBackground>
      </View>
    );
  }
}

export default Object.assign(withAuth(LoginScreen), { navigationOptions });

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  fullScreen: { flex: 1, width: '100%', backgroundColor: 'linearGradient(rgba(0, 0, 0, 0.7))', display: 'flex', alignItems: 'center', justifyContent: 'center' },
  logo: { flex: 1 },
  text: { textAlign: 'left', zIndex: 10000, color: '#fff', width: '100%', marginBottom: 10, width: Dimensions.get('window').width - 100 },
  white: { color: '#fff', marginTop: 20 },
  inputWrapper: { marginTop: 20, marginBottom: 20 },
  input: { width: 200 }
});
