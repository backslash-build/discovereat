import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import navigationOptions from '../navigation/staticNavigationOptions';

function TandCScreen(props) {
  const onPress = () => {
    const navigateAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Home' })]
    });
    props.navigation.dispatch(navigateAction);
  };

  return (
    <TouchableHighlight style={styles.container} onPress={onPress}>
      <Text>TandC Screen</Text>
    </TouchableHighlight>
  );
}

export default Object.assign(TandCScreen, { navigationOptions });

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
