import React from 'react';
import { StyleSheet, Text, View, ScrollView, Dimensions, Platform } from 'react-native';
import ScreenWrapper from '../components/ScreenWrapper';
import { post } from '../api/Api';
import { RecipeCard } from '../components/RecipeCard';
import FullPageLoading from '../components/Loading';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import SingleLineInput from '../components/Forms/SingleLineInput';

export default class DiscoverScreen extends React.Component {
  static navigationOptions = {
    gesturesEnabled: false,
    header: null
  };
  state = {
    loading: true,
    searchTerm: '',
    data: {
      results: [
        {
          id: 219957,
          title: 'Carrot & sesame burgers',
          readyInMinutes: 50,
          image: 'Carrot---sesame-burgers-219957.jpg'
        },
        {
          id: 864633,
          title: 'Banh Mi Burgers with Spicy Sriracha Mayo',
          readyInMinutes: 35,
          image: 'banh-mi-burgers-with-spicy-sriracha-mayo-864633.jpg'
        },
        {
          id: 219871,
          title: 'Halloumi aubergine burgers with harissa relish',
          readyInMinutes: 20,
          image: 'Halloumi-aubergine-burgers-with-harissa-relish-219871.jpg'
        },
        {
          id: 210685,
          title: 'The great breakfast burger',
          readyInMinutes: 115,
          image: 'The-great-breakfast-burger-210685.jpg'
        },
        {
          id: 521885,
          title: 'Pesto & Mozzarella Turkey Burger',
          readyInMinutes: 20,
          image: 'Pesto---Mozzarella-Turkey-Burger-521885.jpg'
        }
      ],
      baseUri: 'https://spoonacular.com/recipeImages/',
      offset: 0,
      number: 25,
      totalResults: 66,
      processingTime: 0,
      expires: 1561085710245,
      isStale: false
    }
  };
  componentDidMount() {
    this.searchRecipes();
  }
  searchRecipes = async () => {
    this.setState({ loading: true });
    try {
      const response = await post('/recipe/search', {
        query: this.state.searchTerm,
        take: '11'
      });
      const res = await response.json();
      if (res && res.success) {
        this.setState({ data: res.data });
      } else {
        this.setState({ error: res.error });
      }
    } catch (error) {
      this.setState({ error });
    }
    this.setState({ loading: false });
  };

  onSearchInputChange = value => {
    clearTimeout(this.searchTimeout);
    this.setState(
      {
        searchTerm: value
      },
      () => {
        this.searchTimeout = setTimeout(() => {
          this.searchRecipes();
        }, 1000);
      }
    );
  };

  render() {
    if (this.state.loading) return <FullPageLoading />;
    const primaryRecipe = this.state.data.results[0];
    return (
      <ScreenWrapper title="Discover" showFooterBar showMenuButton navigation={this.props.navigation}>
        <View style={styles.root}>
          <ScrollView contentContainerStyle={styles.container}>
            <SingleLineInput onChange={this.onSearchInputChange} value={this.state.searchTerm} clearIcon color="#000" />
            {!!this.state.data.totalResults ? (
              <React.Fragment>
                <RecipeCard {...primaryRecipe} imageUrl={`${this.state.data.baseUri}${primaryRecipe.image}`} navigation={this.props.navigation} primary />
                {(this.state.data.results || []).slice(1).map((n, i) => (
                  <RecipeCard key={i} {...n} imageUrl={`${this.state.data.baseUri}${n.image}`} navigation={this.props.navigation} />
                ))}
              </React.Fragment>
            ) : (
              <Text>No results</Text>
            )}
          </ScrollView>
        </View>
      </ScreenWrapper>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    ...ifIphoneX(
      {
        height: Dimensions.get('window').height - 260
      },
      {
        height: Dimensions.get('window').height - 130
      }
    ),
    ...Platform.select({
      android: {
        height: Dimensions.get('window').height - 175
      }
    })
  },
  container: {
    width: Dimensions.get('window').width,
    backgroundColor: '#fff',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    flexDirection: 'row'
    // flex: 1
  },
  search: { color: '#000' }
});
