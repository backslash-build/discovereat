import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import ScreenWrapper from '../components/ScreenWrapper';

export default class DiscoverScreen extends React.Component {
  static navigationOptions = {
    gesturesEnabled: false,
    header: null
  };

  render() {
    return (
      <ScreenWrapper title="Mealpack" showFooterBar showMenuButton navigation={this.props.navigation}>
        <View style={styles.container}>
          <Text>Mealpack</Text>
        </View>
      </ScreenWrapper>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
