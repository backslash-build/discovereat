import React, { useEffect } from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Image, ImageBackground } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import navigationOptions from '../navigation/staticNavigationOptions';
import { withAuth } from '../api/Auth';
import StyledText from '../components/StyledText';
import LandingImage from '../assets/LandingImage';
import Button from '../components/Button';

class SplashScreen extends React.Component {
  state = { touched: false };
  componentDidMount() {
    this.forwardToRoute();
  }

  componentDidUpdate() {
    this.forwardToRoute();
  }

  navigate = screen => {
    const navigateAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: screen })]
    });
    this.props.navigation.dispatch(navigateAction);
  };

  forwardToRoute = () => {
    if (this.props.auth.loading) {
      return;
    } else if (this.props.auth.loggedIn) {
      this.navigate('Home');
    } else if (this.props.auth.error) {
    }
  };

  onPress = () => {
    this.setState({ touched: true });
  };

  renderTouched() {
    return (
      <View style={styles.container}>
        <ImageBackground source={require('../assets/splash.jpg')} style={{ width: '100%', height: '100%' }}>
          <View style={styles.fullScreen}>
            <View style={styles.wrapper}>
              <StyledText variant="h1" style={styles.text}>
                DISCOVER <StyledText style={styles.span}>EAT</StyledText>
              </StyledText>
            </View>
            <View style={styles.touchedWrapper}>
              <Button title="Log In" onPress={() => this.navigate('Login')} />
              <StyledText variant="p2" style={styles.white}>
                Don't have an account?
              </StyledText>
              <Button title="Sign Up" variant="outlined" onPress={() => this.navigate('Register')} />
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }

  renderNotTouched() {
    return (
      <TouchableHighlight style={styles.container} onPress={this.onPress}>
        <ImageBackground source={require('../assets/splash.jpg')} style={{ width: '100%', height: '100%' }}>
          <View style={styles.wrapper}>
            <StyledText variant="h1" style={styles.text}>
              DISCOVER <StyledText style={styles.span}>EAT</StyledText>
            </StyledText>
          </View>
        </ImageBackground>
      </TouchableHighlight>
    );
  }

  render() {
    return this.state.touched ? this.renderTouched() : this.renderNotTouched();
  }
}

export default Object.assign(withAuth(SplashScreen), { navigationOptions });

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  fullScreen: { flex: 1, width: '100%', backgroundColor: 'linearGradient(rgba(0, 0, 0, 0.7))' },
  logo: { flex: 1 },
  text: { textAlign: 'center', alignItems: 'center', zIndex: 10000, color: '#fff', width: 170, marginBottom: 10 },
  wrapper: { position: 'absolute', top: '40%', left: 0, right: 0, textAlign: 'center', alignItems: 'center', zIndex: 10000, color: '#fff', fontSize: 24 },
  span: { fontWeight: 'bold' },
  touchedWrapper: { position: 'absolute', bottom: '10%', width: '100%', display: 'flex', alignItems: 'center', zIndex: 10000, color: '#fff', fontSize: 24 },
  white: { color: '#fff', marginTop: 10, marginBottom: 10 }
});
