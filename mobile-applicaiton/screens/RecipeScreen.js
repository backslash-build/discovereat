import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Platform, TouchableWithoutFeedback } from 'react-native';
import navigationOptions from '../navigation/staticNavigationOptions';
import ScreenWrapper from '../components/ScreenWrapper';
import { get } from '../api/Api';
import FullPageLoading from '../components/Loading';
import StyledText from '../components/StyledText';
import SmallIconButton from '../components/SmallIconButton';
import Button from '../components/Button';
import Tabs from '../components/Tabs';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import { Ionicons, Feather } from '@expo/vector-icons';

class RecipeScreen extends React.Component {
  static navigationOptions = {
    gesturesEnabled: false,
    header: null
  };
  state = {
    loading: true,
    data: {
      vegatarian: false,
      vegan: false,
      glutenFree: false,
      dairyFree: true,
      veryHealthy: false,
      cheap: false,
      veryPopular: false,
      sustainable: false,
      weightWatcherSmartPoints: 9,
      gaps: 'no',
      lowFodmap: false,
      ketogenic: false,
      whole30: false,
      preparationMinutes: 10,
      cookingMinutes: 10,
      sourceUrl: 'http://feedmephoebe.com/2013/11/job-food52s-pan-roasted-cauliflower/',
      spoonacularSourceUrl: 'https://spoonacular.com/on-the-job-pan-roasted-cauliflower-from-food52-479101',
      aggregateLikes: 225,
      spoonacularScore: 97.0,
      healthScore: 46.0,
      creditsText: 'Feed Me Phoebe',
      sourceName: 'Feed Me Phoebe',
      pricePerServing: 199.25,
      extendedIngredients: [
        {
          id: 18079,
          aisle: 'Pasta and Rice',
          image: 'breadcrumbs.jpg',
          name: 'breadcrumbs',
          amount: 0.5,
          unit: 'cup',
          unitShort: null,
          unitLong: null,
          originalString: '1/2 cup fresh breadcrumbs (I used gluten-free!)',
          metaInformation: ['fresh', '(I used gluten-free!)']
        },
        {
          id: 11135,
          aisle: 'Produce',
          image: 'cauliflower.jpg',
          name: 'cauliflower',
          amount: 1.0,
          unit: 'head',
          unitShort: null,
          unitLong: null,
          originalString: '1 head of cauliflower',
          metaInformation: []
        },
        {
          id: 11297,
          aisle: 'Produce;Spices and Seasonings',
          image: 'parsley-curly.png',
          name: 'fresh parsley',
          amount: 1.0,
          unit: 'handful',
          unitShort: null,
          unitLong: null,
          originalString: '1 handful parsley, chopped',
          metaInformation: ['chopped']
        },
        {
          id: 2063,
          aisle: 'Produce',
          image: 'rosemary.jpg',
          name: 'fresh rosemary',
          amount: 2.0,
          unit: 'teaspoons',
          unitShort: null,
          unitLong: null,
          originalString: '2 teaspoons fresh rosemary, chopped',
          metaInformation: ['fresh', 'chopped']
        },
        {
          id: 9297,
          aisle: 'Dried Fruits;Produce;Baking',
          image: 'golden-raisins.jpg',
          name: 'golden raisins',
          amount: 0.25,
          unit: 'cup',
          unitShort: null,
          unitLong: null,
          originalString: '1/4 cup golden raisins, chopped',
          metaInformation: ['chopped']
        },
        {
          id: 4053,
          aisle: 'Oil, Vinegar, Salad Dressing',
          image: 'olive-oil.jpg',
          name: 'olive oil',
          amount: 0.25,
          unit: 'cup',
          unitShort: null,
          unitLong: null,
          originalString: '1/4 cup olive oil, divided',
          metaInformation: ['divided']
        },
        {
          id: 12147,
          aisle: 'Produce;Baking',
          image: 'pine-nuts.png',
          name: 'pine nuts',
          amount: 0.5,
          unit: 'cup',
          unitShort: null,
          unitLong: null,
          originalString: '1/2 cup pine nuts',
          metaInformation: []
        },
        {
          id: 1012047,
          aisle: 'Spices and Seasonings',
          image: 'salt.jpg',
          name: 'sea salt',
          amount: 1.0,
          unit: 'teaspoon',
          unitShort: null,
          unitLong: null,
          originalString: '1 teaspoon sea salt',
          metaInformation: []
        },
        {
          id: 10211111,
          aisle: 'Ethnic Foods;Spices and Seasonings',
          image: 'dried-sumac.jpg',
          name: 'sumac',
          amount: 0.5,
          unit: 'teaspoon',
          unitShort: null,
          unitLong: null,
          originalString: '1/2 teaspoon sumac',
          metaInformation: []
        }
      ],
      id: 479101,
      title: 'On the Job: Pan Roasted Cauliflower From Food52',
      readyInMinute: 0,
      image: 'https://spoonacular.com/recipeImages/479101-556x370.jpg',
      imageType: 'jpg',
      instructions:
        'Cut the florets off the stems and and then chop them into tiny florets. You can also chop up the stems into tiny pieces if you want. You should have about 6 cups of chopped cauliflower. In a large skillet heat 2 tablespoons of olive oil over medium-high heat. Add the cauliflower, 1 teaspoon of salt, rosemary, and sumac. Sauté until cauliflower is tender and starts to brown a bit, stirring as necessary, about 15 minutes. You can also add a bit of olive oil if the pan starts to get too dry or the cauliflower is starting to stick. Meanwhile, in a small skillet, toast the pinenuts over medium heat until golden brown. Set aside. Heat the remaining 2 tablespoons of olive oil in the same pan. Once oil is shimmering, toss in the breadcrumbs and stir, toasting the breadcrumbs. Season with a pinch of kosher salt and a few turns of freshly ground black pepper. Remove from the heat and toss in half of the chopped parsley. When cauliflower is done, remove from the heat and season to taste with freshly ground black pepper and a pinch or so of salt if necessary. Toss in the toasted pine nuts, the chopped raisins, and the remaining parsley. When ready to serve, sprinkle the top with the toasted breadcrumbs and some pecorino.',
      analyzedInstructions: [
        {
          name: '',
          steps: [
            {
              number: 1,
              step:
                'Cut the florets off the stems and and then chop them into tiny florets. You can also chop up the stems into tiny pieces if you want. You should have about 6 cups of chopped cauliflower. In a large skillet heat 2 tablespoons of olive oil over medium-high heat.',
              ingredients: [
                {
                  id: 11135,
                  name: 'cauliflower',
                  image: 'cauliflower.jpg'
                },
                {
                  id: 4053,
                  name: 'olive oil',
                  image: 'olive-oil.jpg'
                }
              ],
              equipment: [
                {
                  id: 404645,
                  name: 'frying pan',
                  image: 'pan.png'
                }
              ]
            },
            {
              number: 2,
              step:
                'Add the cauliflower, 1 teaspoon of salt, rosemary, and sumac. Sauté until cauliflower is tender and starts to brown a bit, stirring as necessary, about 15 minutes. You can also add a bit of olive oil if the pan starts to get too dry or the cauliflower is starting to stick. Meanwhile, in a small skillet, toast the pinenuts over medium heat until golden brown. Set aside.',
              ingredients: [
                {
                  id: 11135,
                  name: 'cauliflower',
                  image: 'cauliflower.jpg'
                },
                {
                  id: 4053,
                  name: 'olive oil',
                  image: 'olive-oil.jpg'
                },
                {
                  id: 12147,
                  name: 'pine nuts',
                  image: 'pine-nuts.png'
                },
                {
                  id: 10211111,
                  name: 'sumac',
                  image: 'dried-sumac.jpg'
                },
                {
                  id: 2047,
                  name: 'salt',
                  image: 'salt.jpg'
                }
              ],
              equipment: [
                {
                  id: 404645,
                  name: 'frying pan',
                  image: 'pan.png'
                }
              ]
            },
            {
              number: 3,
              step:
                'Heat the remaining 2 tablespoons of olive oil in the same pan. Once oil is shimmering, toss in the breadcrumbs and stir, toasting the breadcrumbs. Season with a pinch of kosher salt and a few turns of freshly ground black pepper.',
              ingredients: [
                {
                  id: 18079,
                  name: 'breadcrumbs',
                  image: 'breadcrumbs.jpg'
                },
                {
                  id: 1082047,
                  name: 'kosher salt',
                  image: 'salt.jpg'
                },
                {
                  id: 4053,
                  name: 'olive oil',
                  image: 'olive-oil.jpg'
                }
              ],
              equipment: [
                {
                  id: 404645,
                  name: 'frying pan',
                  image: 'pan.png'
                }
              ]
            },
            {
              number: 4,
              step:
                'Remove from the heat and toss in half of the chopped parsley. When cauliflower is done, remove from the heat and season to taste with freshly ground black pepper and a pinch or so of salt if necessary. Toss in the toasted pine nuts, the chopped raisins, and the remaining parsley. When ready to serve, sprinkle the top with the toasted breadcrumbs and some pecorino.',
              ingredients: [
                {
                  id: 18079,
                  name: 'breadcrumbs',
                  image: 'breadcrumbs.jpg'
                },
                {
                  id: 11135,
                  name: 'cauliflower',
                  image: 'cauliflower.jpg'
                },
                {
                  id: 12147,
                  name: 'pine nuts',
                  image: 'pine-nuts.png'
                },
                {
                  id: 11297,
                  name: 'parsley',
                  image: 'parsley.jpg'
                },
                {
                  id: 2047,
                  name: 'salt',
                  image: 'salt.jpg'
                }
              ],
              equipment: []
            }
          ]
        }
      ],
      nutrition: {
        nutrients: [
          {
            title: 'Calories',
            name: null,
            amount: 349.97,
            unit: 'cal',
            percentOfDailyNeeds: 17.5
          },
          {
            title: 'Fat',
            name: null,
            amount: 26.21,
            unit: 'g',
            percentOfDailyNeeds: 40.32
          },
          {
            title: 'Saturated Fat',
            name: null,
            amount: 2.96,
            unit: 'g',
            percentOfDailyNeeds: 18.51
          },
          {
            title: 'Carbohydrates',
            name: null,
            amount: 26.35,
            unit: 'g',
            percentOfDailyNeeds: 8.78
          },
          {
            title: 'Sugar',
            name: null,
            amount: 9.56,
            unit: 'g',
            percentOfDailyNeeds: 10.62
          },
          {
            title: 'Cholesterol',
            name: null,
            amount: 0.0,
            unit: 'mg',
            percentOfDailyNeeds: 0.0
          },
          {
            title: 'Sodium',
            name: null,
            amount: 725.58,
            unit: 'mg',
            percentOfDailyNeeds: 31.55
          },
          {
            title: 'Protein',
            name: null,
            amount: 7.21,
            unit: 'g',
            percentOfDailyNeeds: 14.42
          },
          {
            title: 'Manganese',
            name: null,
            amount: 1.86,
            unit: 'mg',
            percentOfDailyNeeds: 93.19
          },
          {
            title: 'Vitamin C',
            name: null,
            amount: 71.05,
            unit: 'mg',
            percentOfDailyNeeds: 86.13
          },
          {
            title: 'Vitamin K',
            name: null,
            amount: 57.11,
            unit: 'µg',
            percentOfDailyNeeds: 54.39
          },
          {
            title: 'Folate',
            name: null,
            amount: 103.97,
            unit: 'µg',
            percentOfDailyNeeds: 25.99
          },
          {
            title: 'Vitamin E',
            name: null,
            amount: 3.66,
            unit: 'mg',
            percentOfDailyNeeds: 24.37
          },
          {
            title: 'Phosphorus',
            name: null,
            amount: 193.59,
            unit: 'mg',
            percentOfDailyNeeds: 19.36
          },
          {
            title: 'Magnesium',
            name: null,
            amount: 73.46,
            unit: 'mg',
            percentOfDailyNeeds: 18.36
          },
          {
            title: 'Fiber',
            name: null,
            amount: 4.51,
            unit: 'g',
            percentOfDailyNeeds: 18.04
          },
          {
            title: 'Potassium',
            name: null,
            amount: 630.75,
            unit: 'mg',
            percentOfDailyNeeds: 18.02
          },
          {
            title: 'Vitamin B1',
            name: null,
            amount: 0.27,
            unit: 'mg',
            percentOfDailyNeeds: 17.7
          },
          {
            title: 'Copper',
            name: null,
            amount: 0.35,
            unit: 'mg',
            percentOfDailyNeeds: 17.44
          },
          {
            title: 'Vitamin B6',
            name: null,
            amount: 0.33,
            unit: 'mg',
            percentOfDailyNeeds: 16.35
          },
          {
            title: 'Iron',
            name: null,
            amount: 2.5,
            unit: 'mg',
            percentOfDailyNeeds: 13.87
          },
          {
            title: 'Vitamin B3',
            name: null,
            amount: 2.48,
            unit: 'mg',
            percentOfDailyNeeds: 12.41
          },
          {
            title: 'Vitamin B2',
            name: null,
            amount: 0.2,
            unit: 'mg',
            percentOfDailyNeeds: 11.61
          },
          {
            title: 'Zinc',
            name: null,
            amount: 1.71,
            unit: 'mg',
            percentOfDailyNeeds: 11.43
          },
          {
            title: 'Vitamin B5',
            name: null,
            amount: 1.1,
            unit: 'mg',
            percentOfDailyNeeds: 11.03
          },
          {
            title: 'Calcium',
            name: null,
            amount: 65.87,
            unit: 'mg',
            percentOfDailyNeeds: 6.59
          },
          {
            title: 'Selenium',
            name: null,
            amount: 4.45,
            unit: 'µg',
            percentOfDailyNeeds: 6.36
          },
          {
            title: 'Vitamin A',
            name: null,
            amount: 90.6,
            unit: 'IU',
            percentOfDailyNeeds: 1.81
          }
        ],
        caloricBreakdown: {
          precentProtein: 0.0,
          percentFat: 63.73,
          percentCarbs: 28.48
        },
        weightPerServing: {
          amount: 199.0,
          unit: 'g'
        }
      }
    }
  };
  componentDidMount() {
    this.getRecipe();
  }
  getRecipe = async () => {
    try {
      const response = await get(`/recipe/${this.props.navigation.getParam('id')}`);
      const res = await response.json();
      if (res && res.success) {
        this.setState({ data: res.data });
      } else {
        this.setState({ error: res.error });
      }
    } catch (error) {
      this.setState({ error });
    }
    this.setState({ loading: false });
  };
  onBackPress = () => {
    this.props.navigation.goBack();
  };
  render() {
    if (this.state.loading) return <FullPageLoading />;
    const data = ({ image, title, preparationMinutes, cookingMinutes } = this.state.data);
    return (
      <ScreenWrapper hideHeaderBar showFooterBar showMenuButton navigation={this.props.navigation} onBackPress={this.onBackPress} showBackButton>
        <View style={styles.root}>
          <ScrollView contentContainerStyle={styles.container}>
            <Image style={styles.image} resizeMode="cover" source={{ uri: image }} />
            <TouchableWithoutFeedback onPress={() => this.props.navigation.goBack()}>
              <View style={styles.backArrow}>
                <Ionicons name="md-arrow-back" size={23} color="#000" />
              </View>
            </TouchableWithoutFeedback>
            <View style={styles.header}>
              <View style={styles.row}>
                <StyledText variant="h1" style={styles.title}>
                  {title}
                </StyledText>
                <TouchableWithoutFeedback>
                  <View style={styles.add}>
                    <Feather name="plus" size={23} color="#000" />
                  </View>
                </TouchableWithoutFeedback>
              </View>
              <View style={styles.headerMeta}>
                <View>
                  <StyledText style={styles.mins} variant="p2">{`Prep: ${preparationMinutes} mins`}</StyledText>
                  <StyledText style={styles.mins} variant="p2">{`Cook: ${cookingMinutes} mins`}</StyledText>
                </View>
                <Button title="Add to Mealpack" />
              </View>
            </View>
            <Tabs data={data} />
          </ScrollView>
        </View>
      </ScreenWrapper>
    );
  }
}

export default RecipeScreen;

const styles = StyleSheet.create({
  root: {
    ...ifIphoneX(
      {
        height: Dimensions.get('window').height - 70
      },
      {
        height: Dimensions.get('window').height - 70
      }
    ),
    ...Platform.select({
      android: {
        height: Dimensions.get('window').height - 70
      }
    })
  },
  container: {
    width: Dimensions.get('window').width,
    backgroundColor: '#fff',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    flexDirection: 'row'
    // flex: 1
  },
  image: { width: Dimensions.get('window').width, height: Dimensions.get('window').height / 4 },
  header: { width: Dimensions.get('window').width, margin: 20 },
  fullWidth: { width: Dimensions.get('window').width },
  mins: { width: 100 },
  row: { marginBottom: 20, marginRight: 20, display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: Dimensions.get('window').width - 40 },
  headerMeta: { display: 'flex', flexDirection: 'row', justifyContent: 'space-between', width: Dimensions.get('window').width - 40 },
  backArrow: {
    left: 10,
    top: Dimensions.get('window').height / 4 - 60,
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 10000,
    backgroundColor: 'rgba(255, 255, 255, 0.75)',
    borderRadius: 25
  }
});
