import React, { Component } from 'react';
import { post, token, setToken } from './Api';
import { AsyncStorage } from 'react-native';

const AuthContext = React.createContext('light');

export let absoluteTokenExpiryTime;

export const withAuth = ComponentToWrap => {
  return class AuthComponent extends Component {
    render() {
      return <AuthContext.Consumer>{auth => <ComponentToWrap {...this.props} auth={auth} />}</AuthContext.Consumer>;
    }
  };
};

let interval = null;

export class AuthProvider extends Component {
  state = {
    error: null,
    loading: !!token,
    loggedIn: !!token,
    email: '',
    password: '',
    res: null
  };

  componentDidMount() {
    if (this.state.loggedIn) {
      this._refresh();
    }
  }

  _logout = async () => {
    setToken(null);
    await AsyncStorage.removeItem('discovereat-token');
    // window.clearInterval(interval);
    this.setState({ loggedIn: false, loading: false });
  };

  _save = async json => {
    if (!json.access_token) return Promise.reject(json);
    await setToken(json);
    absoluteTokenExpiryTime = Date.now() + Math.max((token.expires_in || 600) * 1000 - 5000, 5000);
    // window.peopletoken = token;
    await AsyncStorage.setItem('discovereat-token', JSON.stringify(token));
    // window.clearInterval(interval);
    // interval = window.setInterval(this._refresh, 500);
  };

  _login = async (email, password) => {
    try {
      const response = await post('/auth/login', {
        email,
        password
      });
      const res = await response.json();
      if (res && res.success) {
        await this._save(res.data);
        this.setState({ loggedIn: true });
      } else {
        this.setState({ error: res.error });
      }
    } catch (error) {
      this.setState({ error });
    }
    this.setState({ loading: false });
  };

  _register = async (email, password, firstName) => {
    try {
      const response = await post('/auth/register', {
        email,
        password,
        firstName
      });
      const res = await response.json();
      if (res && res.success) {
        await this._save(res.data);
        this.setState({ loggedIn: true });
      } else {
        this.setState({ error: res.error });
      }
    } catch (error) {
      this.setState({ error });
    }
    this.setState({ loading: false });
  };

  _updatePassword = async (email, password, newPassword) => {
    try {
      const response = this._fetch(
        'POST',
        {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        '/auth/update-password',
        JSON.stringify({ email, password, newPassword })
      );

      if (!response.success) {
        this.setState({ loading: false, error: response.error || 'An error occured.' });
        return { success: false, error: response.error };
      }
      this.setState({ loading: false });
      return { success: true };
    } catch (error) {
      this.setState({ loading: false, error });
      return { success: false, error };
    }
  };

  _refresh = async () => {
    if (token && token.expires_in && Date.now() + 5000 < absoluteTokenExpiryTime) {
      return;
    }

    try {
      const response = this._fetch(
        'POST',
        {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        '/auth/token-refresh',
        JSON.stringify({ refreshToken: token.refresh_token })
      );
      console.log(content);

      if (!response.success) {
        this.setState({ loading: false });
        this._logout();
        return;
      }

      if (this.state.loading || !this.state.loggedIn) {
        // make sure that we only call setState if we have anything to change, otherwise we would cause downstream rerenders
        this.setState({ loading: false, loggedIn: true });
      }

      this._save(response.data);
    } catch (error) {
      console.log(error, 'network error');
      this.setState({ loading: true, error });
      window.setTimeout(this._refresh, 5000);
    }
  };

  //   _validateCode = async (uniqueReferralCode, callback) => {
  //     try {
  //       const response = await api.applicantTokenValidate(uniqueReferralCode, {
  //         credentials: 'include'
  //       });
  //       callback(response);
  //     } catch (error) {
  //       callback(false);
  //     }
  //   };

  // _authCode = async (uniqueReferralCode, newPassword, callback) => {
  //   try {
  //     const response = await api.applicantTokenExchange(
  //       { uniqueReferralCode, newPassword },
  //       {
  //         credentials: 'include'
  //       }
  //     );
  //     if (!response.success) {
  //       this.setState({ loading: false, error: response.error || true });
  //       return;
  //     }
  //     this._save(response.data);
  //     this.setState({ loading: false, loggedIn: true }, callback);
  //   } catch (error) {
  //     this.setState({ loading: false, error: error.message });
  //   }
  // };

  // _resetPassword = async (email, callback) => {
  //   try {
  //     const response = await api.resetApplicantPassword(email, {
  //       credentials: 'include'
  //     });
  //     this.setState({ loading: false }, callback);
  //   } catch (error) {
  //     console.log('error', error);
  //     this.setState({ loading: false, error: error.message });
  //   }
  // };

  render() {
    // console.log(token);
    return (
      <AuthContext.Provider
        value={{
          error: this.state.error,
          loading: this.state.loading,
          loggedIn: this.state.loggedIn,
          login: (username, password) => {
            this.setState({ loading: true, error: null });
            this._login(username, password);
          },
          register: (username, password, firstName) => {
            this.setState({ loading: true, error: null });
            this._register(username, password, firstName);
          },
          updatePassword: (username, password, newPassword) => {
            this.setState({ loading: true, error: null });
            this._login(username, password, newPassword);
          },
          logout: () => {
            this._logout();
          }
          //   validateCode: (uniqueReferralCode, callback) => {
          //     this._validateCode(uniqueReferralCode, callback);
          //   },
          //   resetPassword: (email, callback) => {
          //     this.setState({ loading: true });
          //     this._resetPassword(email, callback);
          //   },
          //   authCode: (uniqueReferralCode, newPassword, callback) => {
          //     this.setState({ loading: true });
          //     this._authCode(uniqueReferralCode, newPassword, callback);
          //   },
        }}
      >
        {this.props.children}
      </AuthContext.Provider>
    );
  }
}
