import customError from '../util/customError';
import { AsyncStorage } from 'react-native';

const ADDRESS = 'http://discovereatbackend-env.hrzvuvdkxx.eu-west-2.elasticbeanstalk.com'; //eslint-disable-line

export let token;
try {
  token = JSON.parse(AsyncStorage.getItem('discovereat-token'));
} catch (e) {
  token = null;
}

export async function setToken(value) {
  token = value;
  // await AsyncStorage.setItem('discovereat-token', value);
}

/* ----------- EXCEPTIONS ---------- */
export const UnableToConnectException = customError('UnableToConnectException', 'Unable to connect to the server.');
export const ServerErrorException = customError('ServerErrorException', 'There was an error on the server.');
export const NotFoundException = customError('NotFoundException', 'Endpoint not found.');
export const AuthenticationException = customError('AuthenticationException', 'User is not authenticated.');
export const BadRequestException = customError('BadRequestException', 'Bad request sent.');
export const TimeoutException = customError('TimeoutException', 'The request timed out.');

function getAuthHeader() {
  const value = !!token ? token.access_token : '';
  return 'JWT ' + value;
}

export async function request(path, settings) {
  let response;
  try {
    response = await fetch(`${ADDRESS}${path}`, settings);
  } catch (e) {
    console.log('Unable to connect', e);
    throw new UnableToConnectException();
  }

  if (response.status === 404 || response.status === 500 || response.status === 400 || response.status === 403) {
    trackError({
      path,
      settings,
      response
    });
  }

  if (response.status === 404) throw new NotFoundException();
  if (response.status === 500) throw new ServerErrorException();
  if (response.status === 401) throw new AuthenticationException();
  if (response.status === 403) throw new AuthenticationException();
  if (response.status === 400) throw new BadRequestException(await response.json());

  return response;
}

export async function get(path, body) {
  return await request(path, {
    headers: {
      Authorization: getAuthHeader()
    },
    body
  });
}

export async function del(path) {
  return await request(path, {
    method: 'DELETE',
    headers: {
      Authorization: getAuthHeader()
    }
  });
}

export async function post(path, body) {
  return await request(path, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getAuthHeader()
    },
    body: JSON.stringify(body)
  });
}

export async function patch(path, body) {
  return await request(path, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getAuthHeader()
    },
    body: JSON.stringify(body)
  });
}

export async function put(path, body) {
  return await request(path, {
    method: 'PUT',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
      Authorization: getAuthHeader()
    },
    body: JSON.stringify(body)
  });
}

export async function postFile(path, file) {
  const formData = new FormData();
  formData.append('file', file);

  return await request(path, {
    method: 'POST',
    headers: {
      Accept: 'application/json, text/plain, */*',
      Authorization: getAuthHeader()
    },
    body: formData
  });
}
