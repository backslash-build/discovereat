import React from 'react';
import { Platform, StatusBar, StyleSheet, View, NativeModules, Text } from 'react-native';
import { createAppContainer } from 'react-navigation';
import RootNavigation from './navigation/RootNavigation';
import { AuthProvider } from './api/Auth';

const AppContainer = createAppContainer(RootNavigation);

export default class App extends React.Component {
  state = {
    isLoadingComplete: false
  };

  async componentDidMount() {
    StatusBar.setBarStyle('dark-content', true);

    if (Platform.OS === 'ios') {
      const MAX_FONT_SIZE_MULTIPLIER = 1.118;
      NativeModules.AccessibilityManager.setAccessibilityContentSizeMultipliers({
        extraSmall: 0.823,
        small: 0.882,
        medium: 0.941,
        large: 1.0,
        extraLarge: MAX_FONT_SIZE_MULTIPLIER, // Originally: 1.118,
        extraExtraLarge: MAX_FONT_SIZE_MULTIPLIER, // Originally: 1.235,
        extraExtraExtraLarge: MAX_FONT_SIZE_MULTIPLIER, // Originally: 1.353,
        accessibilityMedium: MAX_FONT_SIZE_MULTIPLIER, // Originally: 1.786,
        accessibilityLarge: MAX_FONT_SIZE_MULTIPLIER, // Originally: 2.143,
        accessibilityExtraLarge: MAX_FONT_SIZE_MULTIPLIER, // Originally: 2.643,
        accessibilityExtraExtraLarge: MAX_FONT_SIZE_MULTIPLIER, // Originally: 3.143,
        accessibilityExtraExtraExtraLarge: MAX_FONT_SIZE_MULTIPLIER // Originally: 3.571
      });
    }
  }

  render() {
    return (
      <AuthProvider>
        <View style={styles.container}>
          <AppContainer />
        </View>
      </AuthProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBarUnderlay: {
    height: 24,
    backgroundColor: 'rgba(0,0,0,0.2)'
  }
});
