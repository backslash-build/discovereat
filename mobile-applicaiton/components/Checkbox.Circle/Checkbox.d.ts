import * as React from 'react';

export interface CheckboxProps {
  outerSize: number;
  filterSize: number;
  innerSize: number;
  outerColor: string;
  filterColor: string;
  innerColor: string;
  styleCheckboxContainer: any;
  onToggle: () => boolean;
  checked: boolean;
  label: string;
  labelPosition: any;
  styleLabel: any;
}

declare const Checkbox: React.ComponentType<CheckboxProps>;

export default Checkbox;
