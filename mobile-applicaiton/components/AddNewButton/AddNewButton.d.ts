import * as React from 'react';

export interface ButtonProps {
  disabled: boolean;
  buttonStyle: any;
  size: string;
  textStyle: any;
  onPress: () => {};
  submitting: boolean;
  title: string;
}

declare const Button: React.ComponentType<ButtonProps>;

export default Button;
