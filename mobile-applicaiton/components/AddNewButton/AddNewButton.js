import React from 'react';
import { Platform, View, TouchableWithoutFeedback, Text, ActivityIndicator, StyleSheet } from 'react-native';
import { FontAwesome, Feather } from '@expo/vector-icons';

export default class AddNewButton extends React.Component {
  selectViewStyle() {
    let style = {
      backgroundColor: '#fff',
      margin: 15,
      borderRadius: 5,
      borderWidth: 2,
      borderColor: '#00A69C',
      zIndex: 50,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingVertical: 20,
      paddingHorizontal: 20
    };

    if (this.props.disabled) {
      style.opacitiy = 0.36;
    }

    return { ...style, ...this.props.buttonStyle };
  }

  selectTextStyle() {
    let style = { color: '#002F2D', fontSize: 18 };

    if (this.props.size == 'small') {
      style.fontSize = 10;
    }

    return { ...style, ...this.props.textStyle };
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={() => this.props.onPress()}>
        <View style={this.selectViewStyle()}>
          <View>{!this.props.submitting && <Text style={this.selectTextStyle()}>{this.props.title}</Text>}</View>
          <View style={styles.addNewIcon}>
            <Feather name="plus" color="#222" size={25} />
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  addNewIcon: {
    height: 35,
    width: 35,
    backgroundColor: '#fff',
    borderWidth: 2,
    borderRadius: 17.5,
    borderColor: '#00A69C',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
