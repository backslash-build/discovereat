import React from 'react';
import { Platform, View, TouchableHighlight, Text, ActivityIndicator } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

export default class SmallIconButton extends React.Component {
  selectViewStyle() {
    let style = {
      backgroundColor: this.props.backgroundColor,
      marginHorizontal: 3,
      borderRadius: 3,
      shadowColor: '#333333',
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowRadius: 3,
      shadowOpacity: 0.5,
      zIndex: 50
    };

    // if (this.props.size === "small") {
    //   style.margin = 5;
    //   style.paddingVertical = 10;
    //   style.padding = 2;
    // }

    return { ...style, ...this.props.buttonStyle };
  }

  selectTextStyle() {
    let style = { color: this.props.color, fontSize: 16, textAlign: 'center' };

    if (this.props.size == 'small') {
      style.fontSize = 10;
    }

    return { ...style, ...this.props.textStyle };
  }

  render() {
    return (
      <View elevation={5} style={this.selectViewStyle()}>
        <TouchableHighlight elevation={5} underlayColor={this.props.underlayColor ? this.props.underlayColor : 'rgb(36, 189, 177)'} onPress={() => this.props.onPress()}>
          <View
            style={{
              padding: 13
            }}
          >
            {!this.props.submitting && (
              <Text style={this.selectTextStyle()}>
                <FontAwesome name={this.props.icon} size={16} color="#fff" />
              </Text>
            )}
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}
