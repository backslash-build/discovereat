import * as React from 'react';

export interface SingleLineInputProps {
  size: string;
  textStyle: any;
  buttonStyle: any;
  color: string;
  backgroundColor: string;
  underlayColor: string;
  submitting: boolean;
  icon: string;
  onPress: () => {};
}

declare const SingleLineInput: React.ComponentType<SingleLineInputProps>;

export default SingleLineInput;
