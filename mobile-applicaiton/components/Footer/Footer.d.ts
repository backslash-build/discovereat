import * as React from 'react';

export interface FooterProps {
  onPress: (screen: string) => {};
  active: string;
}

declare const Footer: React.ComponentType<FooterProps>;

export default Footer;
