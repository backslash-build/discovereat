import React from 'react';
import { Platform, View, TouchableWithoutFeedback, Text, ActivityIndicator, StyleSheet, Dimensions } from 'react-native';
import { MaterialCommunityIcons, Feather, SimpleLineIcons, AntDesign } from '@expo/vector-icons';
import { FooterButton } from '../FooterButton';

export default class Footer extends React.Component {
  onPress = screen => {
    this.props.onPress(screen);
  };
  render() {
    const { active } = this.props;
    return (
      <View style={styles.footer}>
        <View style={styles.footerButtons}>
          <FooterButton
            backgroundColor="#fff"
            icon={<MaterialCommunityIcons name="home-outline" size={24} color={active === 'Home' ? '#FF423D' : '#000'} />}
            color={active === 'Home' ? '#FF423D' : '#000'}
            onPress={() => this.onPress('Home')}
            title=""
            subtitle="Home"
          />
          <FooterButton
            backgroundColor="#fff"
            icon={<Feather name="search" size={24} color={active === 'Discover' ? '#FF423D' : '#000'} />}
            color={active === 'Discover' ? '#FF423D' : '#000'}
            onPress={() => this.onPress('Discover')}
            title=""
            subtitle="Discover"
          />
          <FooterButton
            backgroundColor="#fff"
            icon={<SimpleLineIcons name="social-dropbox" size={24} color={active === 'Mealpack' ? '#FF423D' : '#000'} />}
            color={active === 'Mealpack' ? '#FF423D' : '#000'}
            onPress={() => this.onPress('Mealpack')}
            title=""
            subtitle="Mealpack"
          />
          <FooterButton
            backgroundColor="#fff"
            icon={<SimpleLineIcons name="book-open" size={24} color={active === 'Cookbook' ? '#FF423D' : '#000'} />}
            color={active === 'Cookbook' ? '#FF423D' : '#000'}
            onPress={() => this.onPress('Cookbook')}
            title=""
            subtitle="Cookbook"
          />
          <FooterButton
            backgroundColor="#fff"
            icon={<AntDesign name="setting" size={24} color={active === 'Preferences' ? '#FF423D' : '#000'} />}
            color={active === 'Preferences' ? '#FF423D' : '#000'}
            onPress={() => this.onPress('Preferences')}
            title=""
            subtitle="Preferences"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  footer: {
    backgroundColor: '#fff',
    position: 'absolute',
    bottom: 0,
    left: 0,
    height: 70,
    width: Dimensions.get('window').width,
    borderTopWidth: 1,
    borderColor: '#E8E8E8',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 100,
    overflow: 'visible'
  },
  footerButtons: {
    height: 57,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    ...Platform.select({
      android: {
        height: 100
      }
    })
  },
  footerButtonText: {
    color: '#002F2D'
  }
});
