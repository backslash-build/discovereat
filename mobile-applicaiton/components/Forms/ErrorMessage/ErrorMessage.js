import React from 'react';
import { Platform, View, StyleSheet, Text } from 'react-native';
export default class ErrorMessage extends React.Component {
  getErrorStyle() {
    let style = {
      fontSize: 14,
      marginTop: 10,
      color: 'red',
      textAlign: 'left'
    };

    return { ...style, ...this.props.errorStyle };
  }

  render() {
    return <Text style={this.getErrorStyle()}>{this.props.message}</Text>;
  }
}
