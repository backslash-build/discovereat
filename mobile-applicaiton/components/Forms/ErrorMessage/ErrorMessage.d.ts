import * as React from 'react';

export interface ErrorMessageProps {
  errorStyle: any;
  message: string;
}

declare const ErrorMessage: React.ComponentType<ErrorMessageProps>;

export default ErrorMessage;
