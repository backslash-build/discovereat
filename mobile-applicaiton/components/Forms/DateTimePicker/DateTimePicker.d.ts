import * as React from 'react';

export interface DateTimePickerProps {
  label: string;
  date: string;
  setDate: (date: string) => {};
}

declare const DateTimePicker: React.ComponentType<DateTimePickerProps>;

export default DateTimePicker;
