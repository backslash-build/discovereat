import React from 'react';
import { Platform, View, Button, StyleSheet, Dimensions } from 'react-native';
import RNDatePicker from 'react-native-datepicker';

import Label from '../Label';

export default class DatePicker extends React.Component {
  render() {
    return (
      <View style={styles.element}>
        <Label text={this.props.label} />
        <RNDatePicker
          style={{ width: Dimensions.get('window').width - 30 }}
          date={this.props.date}
          mode="datetime"
          placeholder="select date"
          format="MMMM Do YYYY, h:mma"
          showIcon={false}
          minDate={new Date()}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          onDateChange={date => {
            this.props.setDate(date);
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  element: {
    marginVertical: 20
  }
});
