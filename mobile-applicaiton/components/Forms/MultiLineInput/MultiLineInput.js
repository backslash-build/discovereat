import React from 'react';
import { Platform, View, TextInput, StyleSheet } from 'react-native';

import Label from '../Label';

export default class MultiLineInput extends React.Component {
  render() {
    return (
      <View style={styles.element}>
        <Label text={this.props.label} />
        <TextInput
          maxLength={this.props.maxLength}
          style={styles.textInput}
          secureTextEntry={this.props.secureTextEntry}
          returnKeyType={this.props.returnKeyType}
          onChangeText={value => this.props.onChange(value)}
          underlineColorAndroid="rgba(0,0,0,0)"
          placeholder={this.props.placeholder}
          value={this.props.value}
          blurOnSubmit
          multiline
          editable
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    height: 80,
    borderColor: '#C8C8C8',
    borderWidth: 1,
    backgroundColor: '#fff',
    padding: 5,
    textAlignVertical: 'top'
  }
});
