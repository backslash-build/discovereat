import * as React from 'react';

export interface MultiLineInputProps {
  label: string;
  maxLength: number;
  secureTextEntry: any;
  returnKeyType: any;
  onChange: (value: number) => {};
  placeholder: string;
  value: string;
}

declare const MultiLineInput: React.ComponentType<MultiLineInputProps>;

export default MultiLineInput;
