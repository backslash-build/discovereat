import React from 'react';
import { Platform, View, TextInput, StyleSheet } from 'react-native';

import Label from '../Label';

export default class CreditCardInput extends React.Component {
  state = { focused: false };
  getInputStyle() {
    style = {
      borderColor: '#b3b3b3',
      borderBottomWidth: 2,
      fontSize: 18,
      padding: 5,
      backgroundColor: 'transparent'
    };

    if (this.state.focused) {
      style.borderColor = '#14a99d';
    }

    if (this.props.meta && this.props.meta.error && this.props.meta.touched) {
      style.borderColor = '#e84637';
    }

    return { ...style, ...this.props.inputStyle };
  }

  render() {
    const {
      input: { value, ...input },
      meta: { touched, error, warning },
      inputRef,
      onEnter,
      ...rest
    } = this.props;

    return (
      <View style={styles.element}>
        <Label labelStyle={this.props.labelStyle} text={this.props.label} error={error} touched={touched} />
        <TextInput
          {...input}
          {...rest}
          ref={inputRef}
          value={value
            .replace(/[^\dA-Z]/g, '')
            .replace(/(.{4})/g, '$1 ')
            .trim()}
          multiline={false}
          onFocus={() => this.setState({ focused: true })}
          onBlur={() => this.setState({ focused: false })}
          editable={true}
          style={this.getInputStyle()}
          underlineColorAndroid="rgba(0,0,0,0)"
          blurOnSubmit={true}
          onSubmitEditing={onEnter}
          maxLength={19}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  element: {
    marginVertical: 10
  }
});
