import * as React from 'react';

export interface CheckboxProps {
  meta: MetaProps;
  input: InputProps;
  inputStyle: any;
  labelStyle: any;
  label: string;
  onEnter: () => {};
  inputRef: any;
}
export interface MetaProps {
  touched: boolean;
  error: string;
  warning: string;
}

export interface InputProps {
  value: string;
}

declare const Checkbox: React.ComponentType<CheckboxProps>;

export default Checkbox;
