import * as React from 'react';

export interface LabelProps {
  input: any;
  meta: MetaProps;
}

export interface MetaProps {
  touched: boolean;
  error: string;
  warning: string;
}

declare const Label: React.ComponentType<LabelProps>;

export default Label;
