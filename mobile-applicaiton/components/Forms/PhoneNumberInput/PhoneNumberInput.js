import React from 'react';
import { Platform, View, TextInput, StyleSheet } from 'react-native';
import RNPhoneInput from 'react-native-phone-input';

import Label from '../PhoneNumberInput';
import ErrorMessage from '../ErrorMessage';
import WarningMessage from '../WarningMessage';

export default class PhoneNumberInput extends React.Component {
  state = { focused: false };
  getInputStyle() {
    style = {
      borderColor: '#b3b3b3',
      borderBottomWidth: 2,
      fontSize: 18,
      padding: 5,
      backgroundColor: 'transparent'
    };

    if (this.state.focused) {
      style.borderColor = '#14a99d';
    }

    return style;
  }

  render() {
    const {
      input,
      meta: { touched, error, warning },
      ...rest
    } = this.props;

    return (
      <View style={styles.element}>
        <Label text={this.state.focused + '?'} />
        <RNPhoneInput
          {...rest}
          multiline
          onFocus={() => this.setState({ focused: true })}
          onBlur={() => this.setState({ focused: false })}
          editable={true}
          style={this.getInputStyle()}
          underlineColorAndroid="rgba(0,0,0,0)"
          initialCountry="gb"
        />
        {touched && ((error && <ErrorMessage message={error} />) || (warning && <WarningMessage message={warning} />))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  element: {
    marginVertical: 10
  }
});
