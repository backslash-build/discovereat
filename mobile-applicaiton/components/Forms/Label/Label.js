import React from 'react';
import { Platform, View, StyleSheet, Text } from 'react-native';
export default class Label extends React.Component {
  getStyle() {
    let style = {
      fontSize: 13,
      fontWeight: 'bold',
      marginBottom: 5,
      color: '#8e8e8e'
    };

    return { ...style, ...this.props.labelStyle };
  }

  render() {
    return (
      <Text>
        <Text style={this.getStyle()}>{this.props.text}&nbsp;</Text>
        {this.props.error && this.props.touched && <Text style={styles.error}>&nbsp;{this.props.error}</Text>}
      </Text>
    );
  }
}

const styles = StyleSheet.create({
  error: {
    color: '#e84637',
    marginLeft: 20,
    fontWeight: 'bold'
  }
});
