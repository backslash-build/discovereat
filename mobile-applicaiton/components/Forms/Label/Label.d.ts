import * as React from 'react';

export interface LabelProps {
  labelStyle: any;
  text: string;
  error: string;
  touched: boolean;
}

declare const Label: React.ComponentType<LabelProps>;

export default Label;
