import * as React from 'react';

export interface WarningMessageProps {
  message: string;
}

declare const WarningMessage: React.ComponentType<WarningMessageProps>;

export default WarningMessage;
