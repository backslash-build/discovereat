import React from 'react';
import { Platform, View, StyleSheet, Text } from 'react-native';

export default class WarningMessage extends React.Component {
  render() {
    return <Text style={styles.warningMessage}>{this.props.message}</Text>;
  }
}

const styles = StyleSheet.create({
  warningMessage: {
    fontSize: 17,
    marginBottom: 5,
    color: 'yellow'
  }
});
