import * as React from 'react';

export interface SliderProps {
  label: string;
  value: string;
  unit: string;
  minimumValue: string;
  step: string;
  onChange: (value: string) => {};
}

declare const Slider: React.ComponentType<SliderProps>;

export default Slider;
