import React from 'react';
import { Platform, View, TextInput, StyleSheet, Text } from 'react-native';
import RNSlider from 'react-native-slider';

import Label from '../Label';

export default class Slider extends React.Component {
  render() {
    return (
      <View style={styles.element}>
        <Label text={this.props.label} />
        <View>
          <Text style={styles.value}>{this.props.value + ' ' + this.props.unit}</Text>
        </View>
        <RNSlider value={this.props.value} minimumValue={this.props.minimumValue} maximumValue={this.props.maximumValue} step={this.props.step} onValueChange={value => this.props.onChange(value)} />
        <View style={styles.minimumValue}>
          <Text>{this.props.minimumValue + ' ' + this.props.unit}</Text>
        </View>
        <View style={styles.maximumValue}>
          <Text>{this.props.maximumValue + ' ' + this.props.unit}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  element: {
    marginVertical: 20,
    position: 'relative',
    height: 100
  },
  minimumValue: {
    position: 'absolute',
    left: 0,
    bottom: 0
  },
  maximumValue: {
    position: 'absolute',
    right: 0,
    bottom: 0
  },
  value: {
    textAlign: 'center'
  }
});
