import * as React from 'react';

export interface SingleLineInputProps {
  wrapperStyle: any;
  inputStyle: any;
  label: string;
  onBlur: () => {};
  onChange: (value: number) => {};
  autoCapitalize: boolean;
  value: string;
  clearIcon: boolean;
  keyboardType: string;
  secureTextEntry: boolean;
  color: string;
}

declare const SingleLineInput: React.ComponentType<SingleLineInputProps>;

export default SingleLineInput;
