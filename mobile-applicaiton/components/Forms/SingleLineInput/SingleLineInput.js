import React from 'react';
import { Platform, View, TextInput, StyleSheet, TouchableWithoutFeedback, Dimensions } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

import Label from '../Label';

export default class SingleLineInput extends React.Component {
  state = {
    focused: false
  };

  getWrapperStyle() {
    let style = {
      marginVertical: 10,
      marginHorizontal: 20
    };

    return { ...style, ...this.props.wrapperStyle };
  }

  getInputStyle() {
    let style = {
      borderColor: '#b3b3b3',
      borderBottomWidth: 2,
      fontSize: 18,
      padding: 5,
      backgroundColor: 'transparent',
      width: Dimensions.get('window').width - 100,
      height: 31.5,
      color: this.props.color || '#fff'
    };

    if (this.state.focused) {
      style.borderColor = '#ff423d';
    }

    return { ...style, ...this.props.inputStyle };
  }

  render() {
    return (
      <View style={this.getWrapperStyle()}>
        <Label style={{ color: this.props.color || '#fff' }} text={this.props.label} />
        <View style={{ flexDirection: 'row' }}>
          <TextInput
            multiline={false}
            editable={true}
            style={this.getInputStyle()}
            onFocus={() => this.setState({ focused: true })}
            onBlur={() => {
              this.setState({ focused: false });
              if (this.props.onBlur) {
                this.props.onBlur();
              }
            }}
            onChangeText={value => this.props.onChange(value)}
            autoCapitalize={this.props.autoCapitalize}
            value={this.props.value}
            underlineColorAndroid="rgba(0,0,0,0)"
            clearButtonMode={this.props.clearIcon ? 'always' : 'never'}
            keyboardType={this.props.keyboardType}
            secureTextEntry={this.props.secureTextEntry}
          />
        </View>
      </View>
    );
  }
}
