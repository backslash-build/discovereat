import React from 'react';
import { Platform, View, StyleSheet, Text, TouchableWithoutFeedback, Dimensions } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

import Label from '../Label';

export default class Checkboxes extends React.Component {
  renderCheckboxes() {
    return this.props.options.map((option, index) => {
      let checked = this.props.selectedOptions.indexOf(option.id) !== -1;

      if (checked) {
        return (
          <View key={'vehicle-' + index} style={styles.iconCheck}>
            <TouchableWithoutFeedback onPress={() => this.props.onChange(option.id)}>
              <View style={styles.squareButton}>
                <FontAwesome name={'minus'} size={17} color={'#fff'} style={{ textAlign: 'center' }} />
              </View>
            </TouchableWithoutFeedback>
            <View>
              <Text style={styles.title}>{option.title}</Text>
              <Text style={styles.subtitle}>{option.subtitle}</Text>
            </View>
          </View>
        );
      }
    });
  }

  render() {
    return (
      <View style={styles.element}>
        <Label text={this.props.label} />
        {this.renderCheckboxes()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  element: {
    marginVertical: 20
  },
  iconCheck: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10
  },
  squareButton: {
    height: 35,
    width: 35,
    backgroundColor: '#4d8e90',
    borderRadius: 3,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 7
  },
  checkTextStyle: {
    width: Dimensions.get('window').width - 150,
    fontSize: 17
  },
  title: {},
  subtitle: {
    fontSize: 11
  }
});
