import * as React from 'react';

export interface CheckboxProps {
  options: [OptionProps];
  selectedOptions: OptionProps;
  onChange: (id: string) => {};
  label: string;
}
export interface OptionProps {
  id: string;
  title: string;
  subtitle: string;
}

declare const Checkbox: React.ComponentType<CheckboxProps>;

export default Checkbox;
