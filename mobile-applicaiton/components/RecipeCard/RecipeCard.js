import React from 'react';
import { Platform, TouchableHighlight, StyleSheet, Image, View, Dimensions } from 'react-native';
import StyledText from '../StyledText';

export default class RecipeCard extends React.Component {
  selectViewStyle() {
    let style = {
      alignItems: 'center',
      justifyContent: 'center',
      position: 'relative',
      width: Dimensions.get('window').width / 2 - 2,
      height: 124,
      margin: 0.5
    };

    if (this.props.primary) {
      style.width = Dimensions.get('window').width;
      style.height = 194;
    }

    return style;
  }

  selectImageStyle() {
    let style = { flex: 1, width: Dimensions.get('window').width / 2 };

    if (this.props.primary) {
      style.width = Dimensions.get('window').width;
    }

    return style;
  }

  navigate = () => {
    this.props.navigation.navigate('Recipe', { id: this.props.id });
  };

  render() {
    const { id, image, imageUrl, readyInMinutes, title } = this.props;
    return (
      <TouchableHighlight style={this.selectViewStyle()} underlayColor={'rgb(36, 189, 177)'} onPress={this.navigate}>
        <React.Fragment>
          <StyledText style={styles.text}>{title}</StyledText>
          <Image style={this.selectImageStyle()} resizeMode="cover" source={{ uri: imageUrl }} />
        </React.Fragment>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    width: Dimensions.get('window').width / 2 - 2,
    height: 124,
    margin: 0.5
  },
  logo: { flex: 1, width: Dimensions.get('window').width / 2 },
  text: { position: 'absolute', bottom: 7, left: 7, right: 7, zIndex: 10000, color: '#fff', fontSize: 14 },
  span: { fontWeight: 'bold' }
});
