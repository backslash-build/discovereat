import * as React from 'react';

export interface RecipeCardProps {
  id: string;
  imageUrl: string;
  readyInMinutes: string;
  title: string;
  navigation: any;
  primary: boolean;
}

declare const RecipeCard: React.ComponentType<RecipeCardProps>;

export default RecipeCard;
