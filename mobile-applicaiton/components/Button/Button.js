import React from 'react';
import { Platform, View, TouchableHighlight, Text, ActivityIndicator } from 'react-native';

export default class Button extends React.Component {
  selectViewStyle() {
    let style = {
      backgroundColor: '#FF423D',
      borderRadius: 23,
      width: 220,
      height: 45,
      zIndex: 50,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    };

    if (this.props.variant === 'outlined') {
      style = {
        borderRadius: 23,
        width: 220,
        height: 45,
        zIndex: 50,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 3,
        borderColor: '#fff'
      };
    }

    // if (this.props.size === "small") {
    //   style.margin = 5;
    //   style.paddingVertical = 10;
    //   style.padding = 2;
    // }

    return { ...style, ...this.props.buttonStyle };
  }

  selectTextStyle() {
    let style = { color: '#fff', fontSize: 15, fontWeight: 'bold', textAlign: 'center', width: '100%' };

    if (this.props.size == 'small') {
      style.fontSize = 10;
    }

    return { ...style, ...this.props.textStyle };
  }

  render() {
    return (
      <TouchableHighlight style={this.selectViewStyle()} underlayColor={this.props.underlayColor ? this.props.underlayColor : '#FF423D9D'} onPress={() => this.props.onPress()}>
        <View
          style={{
            width: '100%',
            paddingVertical: 15,
            paddingHorizontal: 20
          }}
        >
          {!this.props.submitting && <Text style={this.selectTextStyle()}>{this.props.title || ''}</Text>}
          {this.props.submitting && <ActivityIndicator animating={true} color={'#fff'} size={'small'} />}
        </View>
      </TouchableHighlight>
    );
  }
}
