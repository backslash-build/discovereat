import * as React from 'react';

export interface ButtonProps {
  size: string;
  title: string;
  textStyle: any;
  buttonStyle: any;
  color: string;
  backgroundColor: string;
  underlayColor: string;
  submitting: boolean;
  onPress: () => {};
  variant: string;
}

declare const Button: React.ComponentType<ButtonProps>;

export default Button;
