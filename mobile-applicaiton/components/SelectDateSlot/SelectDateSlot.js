import React from 'react';
import { Platform, View, TextInput, StyleSheet, Dimensions, Text, TouchableOpacity, TouchableWithoutFeedback, ScrollView, ActivityIndicator } from 'react-native';
import moment from 'moment-timezone';

import Button from '../Button';

export default class SelectDateSlot extends React.Component {
  doNothing() {}

  renderDates() {
    return this.props.dates
      .sort((a, b) => {
        return new Date(a.date) > new Date(b.date);
      })
      .map(mapping => {
        return (
          <Button
            buttonStyle={{ marginBottom: 5 }}
            onPress={() => this.props.onSubmit(mapping.weekNumber)}
            title={moment(mapping.date).format('Do MMMM YYYY')}
            color={'#fff'}
            backgroundColor="#14a99d"
          />
          // <TouchableWithoutFeedback key={"slot-" + mapping.date} >
          //   <View style={{ borderWidth: 1, borderRadius: 3, borderColor: "#555", marginVertical: 5, paddingVertical: 10 }}>
          //     <Text style={{ textAlign: "center", color: "#555" }}></Text>
          //   </View>
          // </TouchableWithoutFeedback>
        );
      });
  }

  render() {
    return (
      <View style={styles.popUpWrapper}>
        <View style={styles.popUpDialog}>
          <ScrollView>
            {this.props.error && <Text style={{ textAlign: 'center' }}>{this.props.error}</Text>}
            <Text style={styles.headline}>{this.props.headline}</Text>
            <Text style={styles.description}>{this.props.description}</Text>
            {!this.props.submitting && this.renderDates()}
            {this.props.submitting && (
              <View style={{ marginVertical: 20, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <ActivityIndicator />
                <Text style={{ textAlign: 'center' }}>Creating subscription...</Text>
              </View>
            )}
            {!this.props.submitting && (
              <TouchableWithoutFeedback onPress={() => this.props.onClose()}>
                <View style={{ marginVertical: 20 }}>
                  <Text style={{ color: '#8e8e8e', textAlign: 'center' }}>None of these</Text>
                </View>
              </TouchableWithoutFeedback>
            )}
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  popUpWrapper: {
    backgroundColor: 'rgba(44, 44, 44, 0.6)',
    ...Platform.select({
      android: {
        height: Dimensions.get('window').height - 80
      },
      ios: {
        height: Dimensions.get('window').height - 60
      }
    }),
    width: Dimensions.get('window').width,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  },
  popUpDialog: {
    backgroundColor: '#fff',
    width: 300,
    borderRadius: 5,
    overflow: 'hidden',
    padding: 10
  },
  headline: {
    marginTop: 15,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold'
  },
  description: {
    marginTop: 10,
    textAlign: 'center',
    fontSize: 13,
    marginBottom: 5
  }
});
