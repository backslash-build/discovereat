import * as React from 'react';

export interface SelectDateSlotProps {
  dates: [any];
  onSubmit: (weekNumber: any) => {};
  error: string;
  headline: string;
  description: string;
  submitting: boolean;
  onClose: () => {};
}

declare const SelectDateSlot: React.ComponentType<SelectDateSlotProps>;

export default SelectDateSlot;
