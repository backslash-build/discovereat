import * as React from 'react';

export interface StyledTextProps {
  variant?: 'h1' | 'h2' | 'h3' | 'p1' | 'p2' | 'p3';
  style: { any };
}

declare const StyledText: React.ComponentType<StyledTextProps>;

export default StyledText;
