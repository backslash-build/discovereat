import React from 'react';
import { Text } from 'react-native';

export default class StyledText extends React.Component {
  selectTextStyle() {
    let style;
    switch (this.props.variant) {
      case 'h1':
        style = { fontSize: 24 };
        break;
      case 'h2':
        style = { fontSize: 17 };
        break;
      case 'h3':
        style = { fontSize: 13, fontWeight: 'bold' };
        break;
      case 'p1':
        style = { fontSize: 13 };
        break;
      case 'p2':
        style = { fontSize: 15 };
        break;
      case 'p3':
        style = { fontSize: 13, fontWeight: 'bold' };
        break;
      default:
        style = {};
        break;
    }
    return { ...style, ...this.props.style };
  }
  render() {
    return <Text {...this.props} style={this.selectTextStyle()} />;
  }
}
