import React from 'react';
import { View, Text, StyleSheet, Dimensions, Keyboard, TouchableWithoutFeedback, Platform, StatusBar } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';

import { HeaderBar } from '../HeaderBar';
import { Footer } from '../Footer';
import { Menu } from '../Menu';
import { withAuth } from '../../api/Auth';

class ScreenWrapper extends React.Component {
  state = {
    hasBeenFocused: false,
    showMenu: false
  };
  componentDidUpdate = () => {
    // if (!this.props.auth.loggedIn) this.resetToScreen('Splash');
  };

  getScreenStyle() {
    let style = {
      flex: 1,
      // height: Dimensions.get('window').height,
      marginTop: 0
      //   paddingTop: 120
    };

    if (this.props.largeTitle) {
      //   style.paddingTop = 180;
      style.marginTop = 0;
    }

    return style;
  }

  getContentStyle() {
    let style = {
      height: Dimensions.get('window').height
    };

    return style;
  }

  goToScreen = screen => {
    this.props.navigation.navigate(screen);
    this.setState({
      showMenu: false
    });
  };

  resetToScreen = screen => {
    const navigateAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: screen })]
    });
    this.props.navigation && this.props.navigation.dispatch(navigateAction);
    this.setState({
      showMenu: false
    });
  };

  logout = async () => {
    await this.props.auth.logout();
    this.setState({
      showMenu: false
    });
  };

  render() {
    const {
      title,
      largeTitle,
      onBackPress,
      onEditPress,
      onEditFinish,
      editMode,
      onNextPress,
      hideNextButton,
      showBackButton,
      showMenuButton,
      showEditButton,
      showSaveButton,
      onSavePress,
      saveSubmitting,
      transparentHeader,
      borderlessHeader,
      backButtonColor,
      children,
      showFooterBar,
      hideHeaderBar
    } = this.props;
    return (
      <View style={styles.container}>
        {!hideHeaderBar && (
          <HeaderBar
            title={title}
            largeTitle={largeTitle}
            onBackPress={onBackPress}
            onEditPress={onEditPress}
            onEditFinish={onEditFinish}
            editMode={editMode}
            onNextPress={onNextPress}
            onOpenMenu={() => this.setState({ showMenu: true })}
            hideNextButton={hideNextButton}
            showBackButton={showBackButton}
            showMenuButton={showMenuButton}
            showEditButton={showEditButton}
            showSaveButton={showSaveButton}
            onSavePress={onSavePress}
            saveSubmitting={saveSubmitting}
            transparentHeader={transparentHeader}
            borderlessHeader={borderlessHeader}
            backButtonColor={backButtonColor}
          />
        )}
        <View style={this.getScreenStyle()}>{children}</View>
        {showFooterBar && <Footer onPress={screen => this.resetToScreen(screen)} active={title} />}
        {this.state.showMenu && <Menu onPress={screen => this.goToScreen(screen)} onClose={() => this.setState({ showMenu: false })} logout={() => this.logout()} />}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAFAFA',
    position: 'relative',
    ...Platform.select({
      android: {
        marginTop: 24
      }
    })
  }
});

export default withAuth(ScreenWrapper);
