import * as React from 'react';
import { HeaderBarProps } from '../HeaderBar';

export interface ScreenWrapperProps extends HeaderBarProps {
  children: any;
  showFooterBar: boolean;
  title: string;
  largeTitle: string;
  transparentHeader: boolean;
  borderlessHeader: boolean;
  showBackButton: boolean;
  showEditButton: boolean;
  editMode: boolean;
  hideNextButton: boolean;
  hideHeaderBar: boolean;
  showMenuButton: boolean;
  showSaveButton: boolean;
  saveSubmitting: boolean;
  backButtonColor: string;
  onBackPress: () => {};
  onEditPress: () => {};
  onEditFinish: () => {};
  onNextPress: () => {};
  onSavePress: () => {};
  navigation: any;
}

declare const ScreenWrapper: React.ComponentType<ScreenWrapperProps>;

export default ScreenWrapper;
