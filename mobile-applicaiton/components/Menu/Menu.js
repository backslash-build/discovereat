import React from 'react';
import { Platform, View, TouchableWithoutFeedback, Text, ActivityIndicator, StyleSheet, Dimensions, Image } from 'react-native';
import { EvilIcons } from '@expo/vector-icons';

import StyledText from '../StyledText/StyledText';
import { withAuth } from '../../api/Auth';

class Menu extends React.Component {
  render() {
    const { onClose, onPress, logout } = this.props;
    return (
      <View style={styles.menu}>
        <View style={styles.menuHeader}>
          <View>
            <Image style={{ width: 100, height: 50 }} resizeMode={'contain'} source={require('../../assets/icon.png')} />
          </View>
          <TouchableWithoutFeedback onPress={onClose}>
            <View>
              <EvilIcons name="close" color="#222" size={30} />
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View>
          <TouchableWithoutFeedback onPress={() => onPress('Home')}>
            <View style={styles.menuLink}>
              <StyledText style={styles.heading}>Home</StyledText>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => onPress('Discover')}>
            <View style={styles.menuLink}>
              <StyledText style={styles.heading}>Discover</StyledText>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => onPress('Mealpack')}>
            <View style={styles.menuLink}>
              <StyledText style={styles.heading}>Mealpack</StyledText>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => onPress('Cookbook')}>
            <View style={styles.menuLink}>
              <StyledText style={styles.heading}>Cookbook</StyledText>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => onPress('Preferences')}>
            <View style={styles.menuLink}>
              <StyledText style={styles.heading}>Preferences</StyledText>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.footer}>
          <TouchableWithoutFeedback onPress={logout}>
            <View style={styles.signOutButton}>
              <StyledText style={styles.faqContactLink}>Sign out</StyledText>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  menu: {
    backgroundColor: '#fff',
    position: 'absolute',
    top: 0,
    left: 0,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    zIndex: 1000,
    padding: 30
  },
  menuHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  heading: {
    color: '#14a99d',
    textAlign: 'center',
    fontSize: 20
  },
  subheading: {
    textAlign: 'center'
  },
  menuLink: {
    marginVertical: 20
  },
  footer: {
    position: 'absolute',
    bottom: 20,
    width: Dimensions.get('window').width,
    alignItems: 'center',
    justifyContent: 'center'
  },
  faqContactLink: {
    textAlign: 'center',
    fontSize: 16
  },
  signOutButton: {
    width: 130,
    borderColor: '#00A69C',
    borderWidth: 2,
    borderRadius: 5,
    padding: 10,
    marginTop: 20
  }
});

export default Menu;
