import * as React from 'react';

export interface MenuProps {
  goToScreen: (screen: string) => {};
  onClose: () => {};
  logout: () => {};
}

declare const Menu: React.ComponentType<MenuProps>;

export default Menu;
