import * as React from 'react';

export interface HeaderBarProps {
  transparentHeader: boolean;
  borderlessHeader: boolean;
  showBackButton: boolean;
  showEditButton: boolean;
  editMode: boolean;
  hideNextButton: boolean;
  showMenuButton: boolean;
  showSaveButton: boolean;
  saveSubmitting: boolean;
  title: string;
  onBackPress: () => {};
  onEditPress: () => {};
  onEditFinish: () => {};
  onNextPress: () => {};
  onSavePress: () => {};
  onOpenMenu: () => {};
}

declare const HeaderBar: React.ComponentType<HeaderBarProps>;

export default HeaderBar;
