import React from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableWithoutFeedback, ActivityIndicator, Platform } from 'react-native';
import { Ionicons, FontAwesome, Entypo } from '@expo/vector-icons';
import { ifIphoneX } from 'react-native-iphone-x-helper';

import StyledText from '../StyledText/StyledText';

export default class HeaderBar extends React.Component {
  state = {
    hasBeenFocused: false
  };

  getHeaderBarStyle() {
    let style = {
      width: Dimensions.get('window').width,
      ...ifIphoneX(
        {
          height: 160
        },
        {
          height: 130
        }
      ),
      position: 'relative',
      top: 0,
      backgroundColor: '#fff',
      padding: 20,
      paddingTop: 60,
      ...Platform.select({
        android: {
          paddingTop: 55,
          height: 83
        }
      }),
      paddingBottom: 10,
      alignItems: 'center',
      justifyContent: 'center',
      zIndex: 1000,
      color: '#4A4A4A'
    };

    if (this.props.red) {
      style.backgroundColor = '#FF3936';
      style.color = '#FFFFFF';
    }

    return style;
  }

  render() {
    const color = this.props.red ? '#fff' : '#4A4A4A';
    return (
      <View style={this.getHeaderBarStyle()}>
        <View style={styles.mainHeaderSection}>
          {!!this.props.showBackButton && (
            <TouchableWithoutFeedback onPress={() => this.props.onBackPress()}>
              <View style={styles.backArrow}>
                <Entypo name="chevron-thin-left" size={23} color={color} />
              </View>
            </TouchableWithoutFeedback>
          )}
          {/* {this.props.showEditButton && this.props.hideBackButton && !this.props.editMode && (
            <TouchableWithoutFeedback onPress={() => this.props.onEditPress()}>
              <View style={styles.editButton}>
                <StyledText style={styles.editButtonText}>Edit</StyledText>
              </View>
            </TouchableWithoutFeedback>
          )}
          {this.props.showEditButton && this.props.hideBackButton && this.props.editMode && (
            <TouchableWithoutFeedback onPress={() => this.props.onEditFinish()}>
              <View style={styles.editButton}>
                <StyledText style={styles.editButtonText}>Done</StyledText>
              </View>
            </TouchableWithoutFeedback>
          )} */}
          {/* {!this.props.hideNextButton && !this.props.showMenuButton && (
            <View style={styles.nextButton}>
              <StyledText onPress={() => this.props.onNextPress()} style={styles.nextButtonText}>
                Next
              </StyledText>
            </View>
          )}
          {this.props.hideNextButton && !this.props.showMenuButton && this.props.showSaveButton && !this.props.saveSubmitting && (
            <View style={styles.nextButton}>
              <StyledText onPress={() => this.props.onSavePress()} style={styles.saveButtonText}>
                Save
              </StyledText>
            </View>
          )}
          {this.props.hideNextButton && !this.props.showMenuButton && this.props.showSaveButton && this.props.saveSubmitting && (
            <View style={styles.nextButton}>
              <ActivityIndicator style={styles.activityIndicator} animating={true} color={'#FAB62D'} size={'small'} />
            </View>
          )} */}

          <View style={styles.titleSection}>
            <View>
              <StyledText bold={true} style={styles.title}>
                {this.props.title}
              </StyledText>
            </View>
          </View>

          {this.props.showMenuButton && (
            <TouchableWithoutFeedback onPress={() => this.props.onOpenMenu()}>
              <View style={styles.menuIcon}>
                <Entypo name="dots-three-horizontal" size={30} color={color} />
              </View>
            </TouchableWithoutFeedback>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backArrow: {
    left: -20,
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'flex-start',
    justifyContent: 'center',
    zIndex: 10000,
    paddingLeft: 10
  },
  editButton: {
    left: -20,
    position: 'absolute',
    color: '#222',
    zIndex: 10000,
    height: 50,
    width: 80,
    paddingLeft: 20,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  editButtonText: {
    color: '#222',
    fontSize: 19
  },
  nextButton: {
    right: -20,
    position: 'absolute',
    zIndex: 1000,
    height: 50,
    width: 80,
    paddingRight: 20,
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  nextButtonText: {
    color: '#222',
    fontSize: 19
  },
  saveButtonText: {
    color: '#222',
    fontSize: 19
  },
  menuIcon: {
    right: -20,
    position: 'absolute',
    height: 50,
    width: 50,
    alignItems: 'flex-end',
    justifyContent: 'center',
    zIndex: 1000,
    paddingRight: 20
  },
  mainHeaderSection: {
    height: 40,
    width: Dimensions.get('window').width - 40,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    ...ifIphoneX(
      {
        top: 60
      },
      {
        top: 30
      }
    ),
    ...Platform.select({
      android: {
        top: 35
      }
    })
  },
  titleSection: {
    height: 50,
    width: Dimensions.get('window').width,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: 17
  }
});
