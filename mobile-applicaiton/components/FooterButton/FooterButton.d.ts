import * as React from 'react';

export interface FooterButtonProps {
  backgroundColor: string;
  color: string;
  disabled: boolean;
  onPress: () => {};
  submitting: boolean;
  title: string;
  subtitle: string;
  icon: any;
}

declare const FooterButton: React.ComponentType<FooterButtonProps>;

export default FooterButton;
