import React from 'react';
import { Platform, TouchableHighlight, Text, View, ActivityIndicator, Image, Dimensions } from 'react-native';

export default class FooterButton extends React.Component {
  selectViewStyle() {
    let style = {
      backgroundColor: this.props.backgroundColor,
      overflow: 'hidden',
      height: 65,
      alignItems: 'center',
      justifyContent: 'center',
      // paddingHorizontal: 20,
      width: Dimensions.get('window').width / 5
    };

    return style;
  }

  selectTextStyle() {
    let style = { color: this.props.color, fontSize: 16, textAlign: 'center' };

    if (this.props.disabled) {
      style.color = '#ccc';
    }

    return style;
  }

  onPress() {
    if (!this.props.disabled) {
      this.props.onPress();
    }
  }

  render() {
    const { submitting, title, subtitle, color, icon } = this.props;
    return (
      <TouchableHighlight onPress={() => this.onPress()} style={this.selectViewStyle()}>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          {icon}
          {!submitting && <Text style={this.selectTextStyle()}>{title.toUpperCase()}</Text>}
          {subtitle && !submitting && <Text style={{ fontSize: 12, textAlign: 'center', color: color }}>{subtitle}</Text>}
          {submitting && <ActivityIndicator animating={true} color={'#fff'} size={'small'} />}
        </View>
      </TouchableHighlight>
    );
  }
}
