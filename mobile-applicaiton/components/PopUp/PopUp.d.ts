import * as React from 'react';

export interface PopUpProps {
  error: string;
  headling: string;
  description: string;
  onSubmit: () => {};
  submitting: boolean;
  buttonText: string;
  onClose: () => {};
}

declare const PopUp: React.ComponentType<PopUpProps>;

export default PopUp;
