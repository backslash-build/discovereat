import React from 'react';
import { Platform, View, TextInput, StyleSheet, Dimensions, Text, TouchableOpacity } from 'react-native';

import Button from '../Button';

export default class PopUp extends React.Component {
  doNothing() {}

  render() {
    return (
      <View style={styles.popUpWrapper}>
        <View style={styles.popUpDialog}>
          {this.props.error && <Text style={{ textAlign: 'center' }}>{this.props.error}</Text>}
          <Text style={styles.headline}>{this.props.headline}</Text>
          <Text style={styles.description}>{this.props.description}</Text>
          <Button
            buttonStyle={{ marginBottom: 5 }}
            onPress={() => this.props.onSubmit()}
            submitting={this.props.submitting}
            title={this.props.buttonText}
            color={'#fff'}
            backgroundColor="#14a99d"
            disabledBackgroundColor="#e6e6e6"
          />
          {this.props.onClose && (
            <Button buttonStyle={{ marginBottom: 5 }} onPress={() => this.props.onClose()} title={'Not right now'} color={'#fff'} backgroundColor="#14a99d" disabledBackgroundColor="#e6e6e6" />
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  popUpWrapper: {
    backgroundColor: 'rgba(44, 44, 44, 0.6)',
    ...Platform.select({
      android: {
        height: Dimensions.get('window').height - 80
      },
      ios: {
        height: Dimensions.get('window').height - 60
      }
    }),
    width: Dimensions.get('window').width,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1000
  },
  popUpDialog: {
    backgroundColor: '#fff',
    width: Dimensions.get('window').width,
    overflow: 'hidden',
    padding: 10
  },
  headline: {
    marginTop: 15,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold'
  },
  description: {
    marginTop: 10,
    textAlign: 'center',
    fontSize: 13
  }
});
