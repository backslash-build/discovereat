import * as React from 'react';

export interface TabsProps {}

declare const Tabs: React.ComponentType<TabsProps>;

export default Tabs;
