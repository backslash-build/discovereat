import React from 'react';
import { Platform, View, Touchabl, TouchableOpacity, ActivityIndicator, StyleSheet } from 'react-native';
import StyledText from '../StyledText';

const Tab = ({ title, onPress, active }) => {
  const selectTextStyle = () => {
    let style = { paddingBottom: 5, borderBottomColor: 'transparent', borderBottomWidth: 2, width: 100, textAlign: 'center', marginBottom: -2 };

    if (active) {
      style.borderBottomColor = '#FF423D';
    }

    return style;
  };
  return (
    <TouchableOpacity onPress={onPress} style={selectTextStyle()}>
      <StyledText variant="h3" style={styles.text}>
        {title}
      </StyledText>
    </TouchableOpacity>
  );
};

export default Tab;

const styles = StyleSheet.create({ text: { textAlign: 'center' } });
