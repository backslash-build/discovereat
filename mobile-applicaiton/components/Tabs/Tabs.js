import React from 'react';
import { Platform, View, TouchableHighlight, Text, ActivityIndicator, StyleSheet, Dimensions } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import Tab from './Tab';
import StyledText from '../StyledText';

export default class SmallIconButton extends React.Component {
  state = { stage: 'INGREDIENTS' };
  onPress = stage => {
    this.setState({ stage });
  };
  renderIngredients = () => {
    return (
      <View style={styles.ingedientRoot}>
        {this.props.data.extendedIngredients.map((n, i) => (
          <StyledText key={i} variant="p2" style={styles.ingedient}>
            {n.originalString}
          </StyledText>
        ))}
      </View>
    );
  };
  renderPreparation = () => {
    return (
      <View style={styles.preparationRoot}>
        {this.props.data.analyzedInstructions[0].steps.map((n, i) => (
          <StyledText key={i} variant="p2" style={styles.ingedient}>
            {`${n.number}. ${n.step}`}
          </StyledText>
        ))}
      </View>
    );
  };
  renderNutrition = () => {
    return (
      <View style={styles.nutritionRoot}>
        {this.props.data.nutrition.nutrients.map((n, i) => (
          <View key={i} style={styles.nutrient}>
            <StyledText variant="p2" style={styles.ingedient}>
              {n.title}
            </StyledText>
            <StyledText variant="p2" style={styles.ingedient}>
              {`${n.amount}${n.unit}`}
            </StyledText>
          </View>
        ))}
      </View>
    );
  };
  render() {
    let content = <View />;
    switch (this.state.stage) {
      case 'INGREDIENTS':
        content = this.renderIngredients();
        break;
      case 'PREPARATION':
        content = this.renderPreparation();
        break;
      case 'NUTRITION':
        content = this.renderNutrition();
        break;
    }
    return (
      <View>
        <View style={styles.tabs}>
          {['INGREDIENTS', 'PREPARATION', 'NUTRITION'].map((n, i) => (
            <Tab key={i} title={n} onPress={() => this.onPress(n)} active={n === this.state.stage} />
          ))}
        </View>
        <View style={styles.content}>{content}</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: { width: Dimensions.get('window').width - 40, marginTop: 30, marginLeft: 20, marginRight: 20, marginBottom: 30 },
  tabs: { display: 'flex', flexDirection: 'row', justifyContent: 'space-around', borderBottomColor: '#EDEDED', borderBottomWidth: 2 },
  ingedientRoot: {},
  ingedient: { marginBottom: 10 },
  nutrient: { display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }
});
