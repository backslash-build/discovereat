import * as React from 'react';

export interface FullPageLoadingProps {}

declare const FullPageLoading: React.ComponentType<FullPageLoadingProps>;

export default FullPageLoading;
