import React from 'react';
import { Platform, ActivityIndicator, StyleSheet, View, Dimensions } from 'react-native';

export default class FullPageLoading extends React.Component {
  getContainerStyle() {
    return {
      height: Dimensions.get('window').height - 65,
      width: Dimensions.get('window').width,
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignItems: 'center'
    };
  }

  render() {
    return (
      <View style={this.getContainerStyle()}>
        <ActivityIndicator animating={true} size={'large'} />
      </View>
    );
  }
}
