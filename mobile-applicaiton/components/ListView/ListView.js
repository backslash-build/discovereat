import React from 'react';
import { Platform, View, Button, StyleSheet, ScrollView, Text, TouchableWithoutFeedback } from 'react-native';

export default class ListView extends React.Component {
  renderItems() {
    if (!this.props.items || this.props.items.length == 0) {
      return (
        <View style={styles.errorContainer}>
          <Text style={styles.noItemsMessage}>{this.props.noItemsMessage}</Text>
        </View>
      );
    }

    return this.props.items.map((item, index) => {
      return <Item key={'item-' + index} onPress={() => this.props.onItemPress()} title={item.title} subtitle={item.subtitle} />;
    });
  }

  render() {
    return <ScrollView style={styles.container}>{this.renderItems()}</ScrollView>;
  }
}

class Item extends React.Component {
  render() {
    return (
      <TouchableWithoutFeedback onPress={() => this.props.onPress()}>
        <View style={styles.item}>
          <Text style={styles.title}>{this.props.title}</Text>
          <Text style={styles.subtitle}>{this.props.subtitle}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff'
  },
  errorContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  noItemsMessage: {
    fontSize: 15
  },
  item: {
    padding: 10,
    height: 100,
    borderBottomWidth: 1,
    borderColor: 'gray'
  },
  title: {
    fontSize: 20
  },
  subtitle: {
    fontSize: 15
  }
});
