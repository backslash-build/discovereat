import * as React from 'react';

export interface ListViewProps {
  items: [ItemProps];
  noItemsMessage: string;
  onItemPress: () => {};
}

export interface ItemProps {
  title: string;
  subtitle: string;
}

declare const ListView: React.ComponentType<ListViewProps>;

export default ListView;
