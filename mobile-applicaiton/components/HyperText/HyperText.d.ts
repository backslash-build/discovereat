import * as React from 'react';
import { StyledTextProps } from '../StyledText';

export interface HyperTextProps extends StyledTextProps {
  style: StyleProps;
  screen: string;
}

export interface StyleProps {
  root: any;
  text: any;
}

declare const HyperText: React.ComponentType<HyperTextProps>;

export default HyperText;
