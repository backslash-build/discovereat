import React from 'react';
import StyledText from '../StyledText';
import { TouchableHighlight } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';

export default class HyperText extends React.Component {
  navigate = () => {
    const navigateAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: this.props.screen })]
    });
    this.props.navigation.dispatch(navigateAction);
  };
  render() {
    const { style, children } = this.props;

    return (
      <TouchableHighlight style={style ? style.root : {}} onPress={this.navigate}>
        <StyledText variant={this.props.variant} style={style ? style.text : {}}>
          {children}
        </StyledText>
      </TouchableHighlight>
    );
  }
}
