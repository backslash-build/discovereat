﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Persistence.Interfaces;

namespace Persistence
{
    public class DatabaseContextFactory : IDatabaseContextFactory<DatabaseContext>
    {
        private readonly DbContextOptions options;

        public DatabaseContextFactory(IOptions<PersistenceOptions> persistenceOptions)
        {
            this.options = new DbContextOptionsBuilder().UseNpgsql(persistenceOptions.Value.ConnectionString).Options;
        }

        public DatabaseContext CreateDbContext()
        {
            return new DatabaseContext(this.options);
        }

        
    }
}
