﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Interfaces
{
    public interface IDatabaseContextFactory<TContext>
    {
        TContext CreateDbContext();
    }
}
