﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence
{
    public class PersistenceOptions
    {
        public string ConnectionString { get; set; }
    }
}
