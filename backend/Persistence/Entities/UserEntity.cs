﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Entities
{
    public class UserEntity
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string PasswordDigest { get; set; }
        public string FirstName { get; set; }
    }
}
