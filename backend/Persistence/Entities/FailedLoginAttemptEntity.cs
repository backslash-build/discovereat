﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Entities
{
    public class FailedLoginAttemptEntity
    {
        public Guid Id { get; set; }
        public DateTime Time { get; set; }
        public Guid UserId { get; set; }
        public string TokenId { get; set; }
    }
}
