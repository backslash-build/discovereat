﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence
{
    public class DesignTimeContextFactory : IDesignTimeDbContextFactory<DatabaseContext>
    {
        public DatabaseContext CreateDbContext(string[] args)
        {
            var connectionString = "Host=localhost;Port=5032;Database=discovereats;Username=discovereats;Password=password";
            var options = new DbContextOptionsBuilder().UseNpgsql(connectionString).Options;
            return new DatabaseContext(options);
        }
    }
}
