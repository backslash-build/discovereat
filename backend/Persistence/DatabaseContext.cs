﻿using Microsoft.EntityFrameworkCore;
using Persistence.Entities;
using System;

namespace Persistence
{
    public class DatabaseContext : DbContext
    {
        internal DatabaseContext()
        {
        }

        internal DatabaseContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<UserEntity> Users { get; set; }
        public DbSet<FailedLoginAttemptEntity> FailedLoginAttempts { get; set; }
    }
}
