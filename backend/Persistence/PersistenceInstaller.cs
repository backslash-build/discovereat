﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Persistence.Interfaces;
using System;

namespace Persistence
{
    public static class PersistenceInstaller
    {
        public static void AddPersistenceServices(this IServiceCollection services, Action<PersistenceOptions> configureOptions)
        {
            services.Configure(configureOptions);
            services.AddSingleton<IDatabaseContextFactory<DatabaseContext>, DatabaseContextFactory>();
        }
    }
}
