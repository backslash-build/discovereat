export PRODUCTION_ECR_ENDPOINT=898582426712.dkr.ecr.eu-west-2.amazonaws.com
export TAG=latest
login="$(aws ecr get-login --no-include-email --region eu-west-2)"
${login}
docker tag app "${PRODUCTION_ECR_ENDPOINT}/discover-eat-backend:${TAG}"
docker push "${PRODUCTION_ECR_ENDPOINT}/discover-eat-backend:${TAG}"
sed -i "s/XBUILDNUMBERX/${TAG}/g" ./Dockerrun.aws.json
sed -i "s/XECR_ENDPOINTX/${ECR_ENDPOINT}/g" ./Dockerrun.aws.json
zip -r output.zip ./Dockerrun.aws.json ./.ebextensions
eb deploy -l "${TAG}-api" DiscoverEatBackend-env --nohang