﻿using System;

namespace Application.Models.User
{
    public class UserModel
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string ReferralCode { get; set; }
    }
}
