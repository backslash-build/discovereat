﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models.Recipe
{
    public class AnalyzedInstructionStepModel
    {
        public int Number { get; set; }
        public string Step { get; set; }
        public List<IngredientModel> Ingredients { get; set; }
        public List<EquipementModel> Equipment { get; set; }
    }
}
