﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models.Recipe
{
    public class NutritionModel
    {
        public List<NutrientModel> Nutrients { get; set; }
        //public List<IngredientNutritionModel> Ingredients { get; set; }
        public CaloricBreakdownModel CaloricBreakdown { get; set; }
        public WeightPerServingModel WeightPerServing { get; set; }
    }
}
