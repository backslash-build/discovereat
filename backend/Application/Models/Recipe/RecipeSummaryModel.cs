﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models.Recipe
{
    public class RecipeSummaryModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int ReadyInMinutes { get; set; }
        public string Image { get; set; }

    }
}
