﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models.Recipe
{
    public class IngredientNutritionModel
    {
        public string Name { get; set; }
        public double Amount { get; set; }
        public string Unit { get; set; }
        public List<NutrientModel> Nutrients { get; set; }
    }
}
