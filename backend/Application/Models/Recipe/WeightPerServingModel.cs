﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models.Recipe
{
    public class WeightPerServingModel
    {
        public double Amount { get; set; }
        public string Unit { get; set; }
    }
}
