﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models.Recipe
{
    public class SearchRecipesQueryModel
    {
        public string Query { get; set; }
        public string Cuisine { get; set; }
        public string Diet { get; set; }
        public string ExcludedIngredients { get; set; }
        public string Intolerances { get; set; }
        public string Take { get; set; }
        public string Skip { get; set; }
        public string Type { get; set; }
        public string LimitiLicense { get; set; }
        public string InstructionsRequired { get; set; }
    }
}
