﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models.Recipe
{
    public class ExtendedIngredientModel
    {
        public long Id { get; set; }
        public string Aisle { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }
        public string Unit { get; set; }
        public string UnitShort { get; set; }
        public string UnitLong { get; set; }
        public string OriginalString { get; set; }
        public List<string> MetaInformation { get; set; }
    }
}
