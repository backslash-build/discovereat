﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models.Recipe
{
    public class NutrientModel
    {
        public string Title { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }
        public string Unit { get; set; }
        public double PercentOfDailyNeeds { get; set; }
    }
}
