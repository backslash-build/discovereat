﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models.Recipe
{
    public class RecipeInformationModel
    {
        public bool Vegatarian { get; set; }
        public bool Vegan { get; set; }
        public bool GlutenFree { get; set; }
        public bool DairyFree { get; set; }
        public bool VeryHealthy { get; set; }
        public bool Cheap { get; set; }
        public bool VeryPopular { get; set; }
        public bool Sustainable { get; set; }
        public int WeightWatcherSmartPoints { get; set; }
        public string Gaps { get; set; }
        public bool LowFodmap { get; set; }
        public bool Ketogenic { get; set; }
        public bool Whole30 { get; set; }
        public int PreparationMinutes { get; set; }
        public int CookingMinutes { get; set; }
        public string SourceUrl { get; set; }
        public string SpoonacularSourceUrl { get; set; }
        public int AggregateLikes { get; set; }
        public double SpoonacularScore { get; set; }
        public double HealthScore { get; set; }
        public string CreditsText { get; set; }
        public string SourceName { get; set; }
        public double PricePerServing { get; set; }
        public List<ExtendedIngredientModel> ExtendedIngredients { get; set; }
        public long Id { get; set; }
        public string title { get; set; }
        public int ReadyInMinute { get; set; }
        public string Image { get; set; }
        public string ImageType { get; set; }
        public string Instructions { get; set; }
        public List<AnalyzedInstructionSetModel> AnalyzedInstructions { get; set; }
        public NutritionModel Nutrition { get; set; }
        public int Servings { get; set; }
    }
}