﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models.Recipe
{
    public class AnalyzedInstructionSetModel
    {
        public string Name { get; set; }
        public List<AnalyzedInstructionStepModel> Steps { get; set; }
    }
}
