﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models.Recipe
{
    public class CaloricBreakdownModel
    {
        public double PrecentProtein { get; set; }
        public double PercentFat { get; set; }
        public double PercentCarbs { get; set; }
    }
}
