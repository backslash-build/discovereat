﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models.Recipe
{
    public class IngredientModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string image { get; set; }
    }
}
