﻿using System;

namespace Application.Models.Auth
{
    /// <summary>
    /// Required: Id and UserType
    /// </summary>
    public class JwtUserModel
    {
        public Guid Id { get; set; }
    }
}
