﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models.Auth
{
    public class RefreshTokenModel
    {
        public string RefreshToken { get; set; }
    }
}
