﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models.Auth
{
    public class AuthResponseDto
    {
        public bool Success { get; set; }
        public Object Data { get; set; }
        public string Error { get; set; }
    }
}
