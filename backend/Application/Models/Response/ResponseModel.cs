﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models.Response
{
    public class ResponseModel
    {
        public bool Success { get; set; }
        public object Data { get; set; }
        public string Error { get; set; }
    }
}
