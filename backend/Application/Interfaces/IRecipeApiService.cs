﻿using Application.Models.Recipe;
using Application.Models.Response;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IRecipeApiService
    {
        Task<ResponseModel> GetRecipeAsync(long recipeId);

        Task<ResponseModel> SearchRecipesAsync(SearchRecipesQueryModel query);
    }
}
