﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces
{
    public interface ICryptoService
    {
        string Hash(string password);
        bool Compare(string password, string hash);
    }
}
