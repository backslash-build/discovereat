﻿using Application.Interfaces;
using Application.Models.Recipe;
using Application.Models.Response;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class RecipeService
    {
        private readonly IRecipeApiService recipeApiService;

        public RecipeService(IRecipeApiService recipeApiService)
        {
            this.recipeApiService = recipeApiService;
        }

        public async Task<ResponseModel> GetRecipeAsync(long recipeId)
        {
            var result = new ResponseModel();
            var recipes = await recipeApiService.GetRecipeAsync(recipeId);

            if (recipes.Success)
            {
                result.Success = true;
                result.Data = recipes.Data;
            }
            else
            {
                result.Success = false;
                result.Error = recipes.Error;
            }

            return result;
        }

        public async Task<ResponseModel> SearchRecipesAsync(SearchRecipesQueryModel Query)
        {
            var result = new ResponseModel();
            var recipes = await recipeApiService.SearchRecipesAsync(Query);

            if (recipes.Success)
            {
                result.Success = true;
                result.Data = recipes.Data;
            }
            else
            {
                result.Success = false;
                result.Error = recipes.Error;
            }

            return result;
        }
    }
}
