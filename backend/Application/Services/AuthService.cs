﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.Models.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Persistence;
using Persistence.Entities;
using Persistence.Interfaces;

namespace Application.Services
{
    public class AuthService
    {
        private readonly ICryptoService cryptoService;
        private readonly IDatabaseContextFactory<DatabaseContext> databaseContextFactory;
        private readonly ILogger<AuthService> logger;
        //private readonly MessagingService messagingService;
        private const int failedLoginPeriodSeconds = 120;
        private const int maxFailedLoginsInPeriod = 5;

        public AuthService(ICryptoService cryptoService, IDatabaseContextFactory<DatabaseContext> databaseContextFactory, ILogger<AuthService> logger)
        {
            this.cryptoService = cryptoService;
            this.databaseContextFactory = databaseContextFactory;
            this.logger = logger;
        }

        /// <summary>
        /// Checks if there are too many failed login attempts in the last period for this user with this device token
        /// </summary>
        /// <param name="tokenId"></param>
        /// <param name="userId"></param>
        /// <returns>true if locked out, false if not</returns>
        public async Task<bool> CheckIfLockedOut(string tokenId, Guid userId)
        {
            using (var context = this.databaseContextFactory.CreateDbContext())
            {
                var failedLoginsInLastPeriod = await context.FailedLoginAttempts.AsNoTracking().CountAsync(a => a.TokenId == tokenId && a.Time > DateTime.UtcNow.AddSeconds(-failedLoginPeriodSeconds));

                return failedLoginsInLastPeriod > maxFailedLoginsInPeriod;
            }
        }
        
        private async Task LogFailedLoginAttempt(string tokenId, Guid userId)
        {
            using (var context = this.databaseContextFactory.CreateDbContext())
            {
                context.FailedLoginAttempts.Add(new FailedLoginAttemptEntity
                {
                    Time = DateTime.UtcNow,
                    TokenId = tokenId,
                    UserId = userId
                });

                await context.SaveChangesAsync();
            }
        }


        public async Task<(bool, UserModel)> AuthenticateUserAsync(string email, string password)
        {
            using (var context = this.databaseContextFactory.CreateDbContext())
            {
                this.logger.LogInformation("finding user with email {UserEmail}", email);

                var user = await context.Users.Where(a => a.Email != null && a.Email.ToLowerInvariant() == email.ToLowerInvariant()).FirstOrDefaultAsync();

                if (user == null || user.PasswordDigest == null)
                {
                    this.logger.LogInformation("found null user in database");

                    return (false, null);
                }

                if (!this.cryptoService.Compare(password, user.PasswordDigest))
                {
                    this.logger.LogInformation("did not match hash");
                    return (false, new UserModel
                    {
                        Email = user.Email,
                        Id = user.Id
                    });
                }
                else
                {
                    this.logger.LogInformation("user success");

                    return (true, new UserModel
                    {
                        Email = user.Email,
                        Id = user.Id
                    });
                }
            }
        }

        public async Task<(bool, UserModel)> RegisterUserAsync(string email, string password, string firstName)
        {
            using (var context = this.databaseContextFactory.CreateDbContext())
            {
                var user = await context.Users.Where(a => a.Email != null && a.Email.ToLowerInvariant() == email.ToLowerInvariant()).FirstOrDefaultAsync();

                if (user != null)
                {
                    this.logger.LogInformation("User tried to register an account with an already registered email account.");

                    return (false, null);
                }

                var hashedPassword = this.cryptoService.Hash(password);
                var newGuid = Guid.NewGuid();

                context.Users.Add(new UserEntity
                {
                    Id = newGuid,
                    Email = email,
                    PasswordDigest = hashedPassword,
                    FirstName = firstName
                });

                await context.SaveChangesAsync();

                this.logger.LogInformation("user registered");

                return (true, new UserModel
                {
                    Email = email,
                    Id = newGuid
                });

            }
        }

        public async Task<(bool, string)> UpdatePasswordAsync(string email, string password, string newPassword)
        {
            using (var context = this.databaseContextFactory.CreateDbContext())
            {
                var user = await context.Users.Where(a => a.Email != null && a.Email.ToLowerInvariant() == email.ToLowerInvariant()).FirstOrDefaultAsync();

                if (user == null)
                {
                    this.logger.LogInformation("User tried to register an account with an already registered email account.");

                    return (false, null);
                }

                if (!this.cryptoService.Compare(password, user.PasswordDigest))
                {
                    this.logger.LogInformation("did not match hash");
                    return (false, "did not match hash");
                }
                else
                {
                    var hashedPassword = this.cryptoService.Hash(newPassword);

                    user.PasswordDigest = hashedPassword;

                    await context.SaveChangesAsync();

                    this.logger.LogInformation("user success");

                    return (true, "");
                }
            }
        }

        //public async Task SendResetApplicantPasswordMessage(string email)
        //{
        //    using (var context = this.databaseContextFactory.CreateDbContext())
        //    {
        //        var applicant = await context.Users.FirstOrDefaultAsync(a => a.Email.ToLowerInvariant() == email.ToLowerInvariant());

        //        if (applicant == null)
        //        {
        //            return;
        //        }

        //        applicant.UniqueReferralCode = RandomCodeGenerator.GenerateWebSafeCode(32);
        //        await context.SaveChangesAsync();

        //        await this.messagingService.DispatchPasswordResetToApplicant(applicant.Id);


        //        //var baseUrl = Environment.GetEnvironmentVariable("App__ApiBaseUrl");

        //        //var fromEmail = applicant?.Source?.FromEmail != null
        //        //    ? applicant.Source.FromEmail
        //        //    : $"People Compliance <{Environment.GetEnvironmentVariable("App__SendingEmail")}>";

        //        //(string htmlBody, string txtBody) = await EmailRenderingUtil.RenderPasswordResetEmail(new PasswordResetEmailTemplateModel
        //        //{
        //        //    Name = applicant.FirstName,
        //        //    ReferralLink = $"{Environment.GetEnvironmentVariable("App__ApplicantPortalBaseUrl")}/refer?code={applicant.UniqueReferralCode}&email={applicant.Email}",
        //        //    UnsubscribeLink = baseUrl + "/auth/email/unsubscribe/applicant/code=" + applicant.UniqueReferralCode
        //        //});

        //        //await context.SaveChangesAsync();

        //        //await this.emailService.SendEmail(
        //        //    new List<string> { applicant.Email },
        //        //    new List<string> { fromEmail },
        //        //    fromEmail,
        //        //    "Reset your password",
        //        //    htmlBody,
        //        //    txtBody
        //        //);
        //    }

        //}

    }
}
