﻿using Microsoft.EntityFrameworkCore;
using Persistence;
using Persistence.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Application
{
    public class Class1
    {
        private readonly IDatabaseContextFactory<DatabaseContext> databaseContextFactory;

        public Class1(IDatabaseContextFactory<DatabaseContext> databaseContextFactory)
        {
            this.databaseContextFactory = databaseContextFactory;
        }

        public async Task<bool> MyTestMethod()
        {
            using (var context = this.databaseContextFactory.CreateDbContext())
            {
                return await context.Users.AnyAsync();
            }
        }
     }
}
