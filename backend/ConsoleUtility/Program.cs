﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Persistence;
using Persistence.Interfaces;
using System;
using System.IO;
using System.Linq;

namespace ConsoleUtility
{
    public class Program
    {
        static void Main(string[] args)
        {
            var basePath = PlatformServices.Default.Application.ApplicationBasePath;
            var dotenvPath = Path.Combine(basePath, "ConsoleUtility.env");
            DotNetEnv.Env.Load(clobberExistingVars: false, path: dotenvPath);

            IServiceCollection services = new ServiceCollection();
            services.AddPersistenceServices(options =>
            {
                options.ConnectionString = Environment.GetEnvironmentVariable("DatabaseConnectionString");
            });
            var serviceProvider = services.BuildServiceProvider();

            var factory = serviceProvider.GetService<IDatabaseContextFactory<DatabaseContext>>();

            using (var context = factory.CreateDbContext())
            {
                var pendingMigrations = context.Database.GetPendingMigrations();
                Console.WriteLine($"Pending migrations: ({pendingMigrations.Count()})");
                foreach (var m in pendingMigrations)
                {
                    Console.WriteLine(m);
                }
                Console.WriteLine();
                Console.WriteLine();

                var appliedMigrations = context.Database.GetAppliedMigrations();
                Console.WriteLine($"Applied migrations: ({appliedMigrations.Count()})");
                foreach (var m in appliedMigrations)
                {
                    Console.WriteLine(m);
                }
                Console.WriteLine();

                Console.WriteLine("Migrating...");
                context.Database.Migrate();
                Console.WriteLine("Done.");
            }
        }
    }
}
