﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Application.Interfaces;

namespace Api.Services
{
    public class CryptoService: ICryptoService
    {

        private byte[] Hash(string password, byte[] salt)
        {
            return KeyDerivation.Pbkdf2(
                password,
                salt,
                KeyDerivationPrf.HMACSHA256,
                10000,
                256 / 8);
        }

        public string Hash(string password)
        {
            var saltBytes = new byte[128 / 8];

            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(saltBytes);
            }

            var hashBytes = this.Hash(password, saltBytes);

            return $"{Convert.ToBase64String(saltBytes)}.{Convert.ToBase64String(hashBytes)}";
        }

        public bool Compare(string password, string hash)
        {
            var inputs = hash.Split('.');
            var saltBytes = Convert.FromBase64String(inputs[0]);
            var hashString = inputs[1];
            var saltedPassword = Convert.ToBase64String(this.Hash(password, saltBytes));
            return saltedPassword == hashString;
        }
    }
}
