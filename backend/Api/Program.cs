﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.PlatformAbstractions;

namespace Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var basePath = PlatformServices.Default.Application.ApplicationBasePath;
            var dotenvPath = Path.Combine(basePath, "Api.env");
            DotNetEnv.Env.Load(clobberExistingVars: false, path: dotenvPath);

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
