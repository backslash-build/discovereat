﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Application.Models.Auth;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace Api.Authentication
{
    public static class AuthenticationExtensions
    {
        public static void AddAuthenticationServices(this IServiceCollection services, IConfiguration configuration)
        {

            services.AddSingleton<JwtSecurityTokenHandler>();

            var configurationSection = configuration;
            services.Configure<JwtOptions>(configurationSection);
            services.AddSingleton<JwtService>();

            var jwtOptions = new JwtOptions();
            configurationSection.Bind(jwtOptions);

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.Events = new JwtBearerEvents
                {
                    OnTokenValidated = async context =>
                    {
                        var id = context.Principal.Claims.Where(c => c.Type == JwtClaimTypes.UserId).FirstOrDefault()?.Value;

                        //Sentry.SentrySdk.ConfigureScope(s => s.User = new Sentry.Protocol.User
                        //{
                        //    Id = id
                        //});
                    }
                };

                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = jwtOptions.JwtIssuer,

                    ValidateAudience = true,
                    ValidAudience = JwtAudienceTypes.AccessToken,

                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.JwtSigningKey)),

                    ValidateLifetime = true,

                    NameClaimType = JwtClaimTypes.NameIdentifier,

                    RoleClaimType = JwtClaimTypes.Role
                };


            });
        }

        public static JwtUserModel GetJwtUser(this ClaimsPrincipal user, ILogger logger)
        {
            return new JwtUserModel
            {
                Id = user.GetUserId(logger),
            };
        }

        public static Guid GetUserId(this ClaimsPrincipal user, ILogger logger)
        {
            try
            {
                var stringId = user.Claims.First(claim => claim.Type == JwtClaimTypes.UserId).Value;
                //var stringId = user.Identity.Name.Claims.First(claim => claim.Type == JwtClaimTypes.Subject).Value;
                var guidId = new Guid(stringId);
                return guidId;
            }
            catch (Exception e)
            {
                //logger.LogInformation(LogStrings.ExceptionOccured, "AuthenticationExtensions", "GetUserId", e.Message);
                return Guid.Empty;
            }
        }

        //public static UserType GetUserType(this ClaimsPrincipal user, ILogger logger)
        //{
        //    try
        //    {
        //        return (UserType)Int32.Parse(user.Claims.First(claim => claim.Type == JwtClaimTypes.UserType).Value);
        //    }
        //    catch (Exception e)
        //    {
        //        logger.LogInformation(LogStrings.ExceptionOccured, "AuthenticationExtensions", "GetUserType", e.Message);
        //        return 0;
        //    }
        //}
    }
}
