﻿namespace Api.Authentication
{
    public static class JwtAudienceTypes
    {
        public const string AccessToken = "access";
        public const string RefreshToken = "refresh";
        public const string DeviceCookie = "device";
        public const string SignedUrl = "signed_url";
    }
}
