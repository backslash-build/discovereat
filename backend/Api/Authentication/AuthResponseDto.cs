﻿using Newtonsoft.Json;
using System;

namespace Api.Authentication
{
    public class JwtAuthResponseDto
    {
        public bool Success { get; set; }
        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }
        [JsonProperty(PropertyName = "refresh_token")]
        public string RefreshToken { get; set; }
        [JsonProperty(PropertyName = "expires_in")]
        public int ExpiresIn { get; set; }        
        public Guid UserId { get; set; }
        // public UserType UserType { get; set; }
    }
}
