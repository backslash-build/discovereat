﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using Application.Models.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Api.Authentication
{
    public class JwtService
    {
        private readonly JwtSecurityTokenHandler jwtSecurityTokenHandler;
        private readonly JwtOptions jwtOptions;
        private readonly JwtHeader jwtHeader;
        private readonly TokenValidationParameters refreshTokenValidationParameters;
        private readonly TokenValidationParameters deviceCookieValidationParameters;
        private readonly TokenValidationParameters signedRequestValidationParameters;
        private const int accessTokenExpiryMinutes = 10;
        private const int refreshTokenExpiryMinutes = 100 * 24 * 60;
        private readonly CookieOptions deviceCookieOptions;

        public JwtService(IOptions<JwtOptions> jwtOptions, JwtSecurityTokenHandler jwtSecurityTokenHandler)
        {
            this.jwtSecurityTokenHandler = jwtSecurityTokenHandler;
            this.jwtOptions = jwtOptions.Value;

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.jwtOptions.JwtSigningKey));
            this.jwtHeader = new JwtHeader(new SigningCredentials(key, SecurityAlgorithms.HmacSha256));
            this.refreshTokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = this.jwtOptions.JwtIssuer,

                ValidateAudience = true,
                ValidAudience = JwtAudienceTypes.RefreshToken,

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = key,

                ValidateLifetime = true,

                NameClaimType = JwtClaimTypes.NameIdentifier,

                RoleClaimType = JwtClaimTypes.Role
            };

            this.deviceCookieValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = this.jwtOptions.JwtIssuer,

                ValidateAudience = true,
                ValidAudience = JwtAudienceTypes.DeviceCookie,

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = key,

                ValidateLifetime = false,

                NameClaimType = JwtClaimTypes.NameIdentifier,

                RoleClaimType = JwtClaimTypes.Role
            };

            this.signedRequestValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = this.jwtOptions.JwtIssuer,

                ValidateAudience = true,
                ValidAudience = JwtAudienceTypes.SignedUrl,

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = key,

                ValidateLifetime = true
            };

            this.deviceCookieOptions = new CookieOptions
            {
                HttpOnly = true
            };
        }

        private string Create(string sub, int? durationInMinutes, /*UserType userType,*/ string audience)
        {
            var nowUtc = DateTime.UtcNow;
            var centuryBegin = new DateTime(1970, 1, 1);
            var now = (long)(new TimeSpan(nowUtc.Ticks - centuryBegin.Ticks).TotalSeconds);
            var issuer = this.jwtOptions.JwtIssuer ?? string.Empty;


            var payload = new JwtPayload
            {
                { JwtClaimTypes.UserId, sub }, 
                { JwtClaimTypes.NameIdentifier, sub },
                { JwtClaimTypes.Issuer, issuer },
                { JwtClaimTypes.Audience, audience },
                { JwtClaimTypes.IssuedAt, now },
                { JwtClaimTypes.NotBefore, now },
                { JwtClaimTypes.TokenId, Guid.NewGuid().ToString("N") },
                // { JwtClaimTypes.UserType, (int) userType },
                //{ JwtClaimTypes.Role, (int) userType }
            };

            if (durationInMinutes.HasValue)
            {
                var expires = nowUtc.AddMinutes(durationInMinutes.Value);
                var exp = (long)(new TimeSpan(expires.Ticks - centuryBegin.Ticks).TotalSeconds);
                payload[JwtClaimTypes.Expiry] = exp;
            }

            var jwt = new JwtSecurityToken(this.jwtHeader, payload);
            var token = this.jwtSecurityTokenHandler.WriteToken(jwt);
            return token;
        }

        public JwtAuthResponseDto GenerateTokensForUser(JwtUserModel userModel)
        {
            return new JwtAuthResponseDto
            {
                Success = true,
                AccessToken = this.Create(userModel.Id.ToString(), accessTokenExpiryMinutes, /*userModel.UserType,*/ JwtAudienceTypes.AccessToken),
                RefreshToken = this.Create(userModel.Id.ToString(), refreshTokenExpiryMinutes, /*userModel.UserType,*/ JwtAudienceTypes.RefreshToken),
                ExpiresIn = accessTokenExpiryMinutes * 60,
                UserId = userModel.Id,
                // UserType = userModel.UserType
            };
        }

        public string GenerateSignedRequestToken(string url, int? durationInMinutes)
        {
            var nowUtc = DateTime.UtcNow;
            var centuryBegin = new DateTime(1970, 1, 1);
            var now = (long)(new TimeSpan(nowUtc.Ticks - centuryBegin.Ticks).TotalSeconds);
            var issuer = this.jwtOptions.JwtIssuer ?? string.Empty;

            var payload = new JwtPayload
            {
                { JwtClaimTypes.SignedUrl, url },
                { JwtClaimTypes.Issuer, issuer },
                { JwtClaimTypes.Audience, JwtAudienceTypes.SignedUrl },
                { JwtClaimTypes.IssuedAt, now },
                { JwtClaimTypes.NotBefore, now },
                { JwtClaimTypes.TokenId, Guid.NewGuid().ToString("N") }
            };

            if (durationInMinutes.HasValue)
            {
                var expires = nowUtc.AddMinutes(durationInMinutes.Value);
                var exp = (long)(new TimeSpan(expires.Ticks - centuryBegin.Ticks).TotalSeconds);
                payload[JwtClaimTypes.Expiry] = exp;
            }

            var jwt = new JwtSecurityToken(this.jwtHeader, payload);
            var token = this.jwtSecurityTokenHandler.WriteToken(jwt);
            return token;
        }

        //public bool ValidateSignedRequest(PathString pathString, string tokenParam)
        //{
        //    if (string.IsNullOrWhiteSpace(tokenParam))
        //    {
        //        return false;
        //    }

        //    try
        //    {
        //        this.jwtSecurityTokenHandler.ValidateToken(tokenParam, this.signedRequestValidationParameters, out var token);

        //        var tokenSignedPath = (token as JwtSecurityToken).Claims.First(claim => claim.Type == JwtClaimTypes.SignedUrl).Value;

        //        if (tokenSignedPath != pathString.Value || string.IsNullOrEmpty(tokenSignedPath))
        //        {
        //            return false;
        //        }

        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        return false;
        //    }

        //}

        //public void SetDeviceCookie(IResponseCookies cookies, JwtUserModel user)
        //{

        //    cookies.Append($"device_cookie_{user.UserType.ToString()}", this.Create(user.Id.ToString(), null, user.UserType, JwtAudienceTypes.DeviceCookie), this.deviceCookieOptions);
        //}

        //public bool ValidateDeviceCookie(IRequestCookieCollection cookies, JwtUserModel user, out string tokenId)
        //{
        //    var cookie = cookies[$"device_cookie_{user.UserType.ToString()}"];
        //    tokenId = null;

        //    if (string.IsNullOrWhiteSpace(cookie))
        //    {
        //        return false;
        //    }

        //    try
        //    {
        //        this.jwtSecurityTokenHandler.ValidateToken(cookie, this.deviceCookieValidationParameters, out var token);
        //        var claimUserId = (token as JwtSecurityToken).Subject;

        //        if (claimUserId == user.Id.ToString())
        //        {
        //            tokenId = (token as JwtSecurityToken).Claims.First(claim => claim.Type == JwtClaimTypes.TokenId).Value;
        //            return true;
        //        }
        //        return false;
        //    }
        //    catch //(Exception e)
        //    {
        //        return false;
        //    }
        //}

        //TODO move this into another service accesible to multiple controllers
        public bool ValidateRefreshToken(RefreshTokenModel refreshTokenModel, out JwtUserModel user)
        {
            user = null;

            if (string.IsNullOrWhiteSpace(refreshTokenModel.RefreshToken))
            {
                return false;
            }

            try
            {
                this.jwtSecurityTokenHandler.ValidateToken(refreshTokenModel.RefreshToken, this.refreshTokenValidationParameters, out var token);

                var claimUserId = (token as JwtSecurityToken).Subject;
                // var claimUserType = (token as JwtSecurityToken).Claims.First(claim => claim.Type == JwtClaimTypes.UserType).Value;

                var userId = Guid.Parse(claimUserId);
                // var userType = (UserType)Int32.Parse(claimUserType);

                user = new JwtUserModel
                {
                    Id = userId,
                    // UserType = userType
                };
                return true;

            }
            catch //(Exception e)
            {
                return false;
            }
        }
    }
}