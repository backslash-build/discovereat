﻿namespace Api.Authentication
{
    public static class JwtClaimTypes
    {
        public const string UserId = "user_id";
        public const string Issuer = "iss";
        public const string Audience = "aud";
        public const string IssuedAt = "iat";
        public const string NotBefore = "nbf";
        public const string Expiry = "exp";
        public const string TokenId = "jti";
        public const string UserType = "user_type";
        public const string NameIdentifier = "sub";
        public const string Role = "role";
        public const string SignedUrl = "signed_url";
    }
}
