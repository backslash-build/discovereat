﻿namespace Api.Authentication
{
    public class JwtOptions
    {
        public string JwtSigningKey { get; set; }
        public string JwtIssuer { get; set; }
        public int JwtExpiryDays { get; set; }
    }
}