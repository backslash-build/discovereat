﻿using Application.Models.Recipe;
using Application.Models.Response;
using Application.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RecipeController : ControllerBase
    {
        public readonly RecipeService recipeService;

        public RecipeController(RecipeService recipeService)
        {
            this.recipeService = recipeService;
        }

        [HttpGet("{recipeId}")]
        public async Task<ResponseModel> GetRecipeAsync(long recipeId)
        {
            var recipes = await recipeService.GetRecipeAsync(recipeId);

            return recipes;
        }

        [HttpPost("search")]
        public async Task<ResponseModel> SearchRecipesAsync([FromBody] SearchRecipesQueryModel Query)
        {
            var recipes = await recipeService.SearchRecipesAsync(Query);

            return recipes;
        }
    }
}
