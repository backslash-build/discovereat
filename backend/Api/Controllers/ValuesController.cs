﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Persistence.Interfaces;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly Class1 class1;
        private readonly IDatabaseContextFactory<DatabaseContext> databaseContextFactory;

        public ValuesController(Class1 class1, IDatabaseContextFactory<DatabaseContext> databaseContextFactory)
        {
            this.class1 = class1;
            this.databaseContextFactory = databaseContextFactory;
        }
#if DEBUG
        // GET api/values
        [HttpGet]
        public ActionResult<string> Get()
        {
            //using (var context = this.databaseContextFactory.CreateDbContext())
            //{
            //    context.Database.EnsureDeleted();
            //    context.Database.Migrate();

            //    context.Users.Add(new Persistence.Entities.UserEntity
            //    {
            //        Id = Guid.NewGuid()
            //    });

            //    context.SaveChanges();
            //}

            return "Database created.";
        }
#endif
        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<string>> Get(int id)
        {
            if(await this.class1.MyTestMethod())
            {
                return "There is things in the DB";
            }

            return "The DB is empty";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
