﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Api.Authentication;
using Application.Interfaces;
using Application.Models.Auth;
using Application.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Persistence;
using Persistence.Interfaces;

namespace Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly AuthService authService;
        private readonly JwtService jwtService;
        private readonly ICryptoService cryptoService;
        private readonly JwtSecurityTokenHandler jwtSecurityTokenHandler;
        private readonly IDatabaseContextFactory<DatabaseContext> databaseContextFactory;
        private readonly ILogger<AuthController> logger;

        public AuthController(
            AuthService authService,
            JwtService jwtService,
            ICryptoService cryptoService,
            JwtSecurityTokenHandler jwtSecurityTokenHandler,
            IDatabaseContextFactory<DatabaseContext> databaseContextFactory,
            ILogger<AuthController> logger
            )
        {
            this.authService = authService;
            this.jwtService = jwtService;
            this.cryptoService = cryptoService;
            this.jwtSecurityTokenHandler = jwtSecurityTokenHandler;
            this.databaseContextFactory = databaseContextFactory;
            this.logger = logger;
        }

        [HttpPost("login")]
        public async Task<AuthResponseDto> LoginAsync([FromBody] LoginDto authValues)
        {
            var (pass, UserModel) = await this.authService.AuthenticateUserAsync(authValues.Email, authValues.Password);

            var jwtUserModel = UserModel != null ? new JwtUserModel
            {
                Id = UserModel.Id,
            } : null;


            if (UserModel == null)
            {
                return new AuthResponseDto
                {
                    Success = false,
                    Error = "User doesn't exist."
                };
            }

            if (!pass)
            {
                return new AuthResponseDto
                {
                    Success = false,
                    Error = "Invalid credentials."
                };
            }

            var tokens = this.jwtService.GenerateTokensForUser(jwtUserModel);

            return new AuthResponseDto
            {
                Success = true,
                Data = tokens
            };
        }

        [HttpPost("register")]
        public async Task<AuthResponseDto> RegisterAccountAsync([FromBody] RegisterDto authValues)
        {
            var (pass, UserModel) = await this.authService.RegisterUserAsync(authValues.Email, authValues.Password, authValues.FirstName);

            var jwtUserModel = UserModel != null ? new JwtUserModel
            {
                Id = UserModel.Id,
            } : null;

            if (!pass)
            {
                return new AuthResponseDto
                {
                    Success = false,
                    Error = "Could not register email address."
                };
            }

            var tokens = this.jwtService.GenerateTokensForUser(jwtUserModel);

            return new AuthResponseDto
            {
                Success = true,
                Data = tokens
            };
        }

        [HttpPost("update-password")]
        public async Task<AuthResponseDto> UpdatePasswordAsync([FromBody] UpdatePasswordDto authValues)
        {
            var (pass, Error) = await this.authService.UpdatePasswordAsync(authValues.Email, authValues.Password, authValues.NewPassword);

            if (!pass)
            {
                return new AuthResponseDto
                {
                    Success = false,
                    Error = Error
                };
            }

            return new AuthResponseDto
            {
                Success = true
            };
        }

        //[HttpPost("reset-password")]
        //public async Task<bool> ResetPassword(string email)
        //{
        //    await this.authService.SendResetApplicantPasswordMessage(email);
        //    return true;
        //}

        //[HttpPost("token-exchange")]
        //public async Task<AuthResponseDto> TokenExchange([FromBody] ApplicantTokenExchangeRequestModel requestValues)
        //{
        //    if (requestValues.UniqueReferralCode == null)
        //    {
        //        return new AuthResponseDto
        //        {
        //            Success = false,
        //            Error = "No referral code specified."
        //        };
        //    }

        //    //validate the new password
        //    if (!InputValidator.ValidatePassword(requestValues.NewPassword))
        //    {
        //        return new AuthResponseDto
        //        {
        //            Success = false,
        //            Error = "New password must be a minimum of 8 characters, contain a mixture of lowercase and uppercase and at least one special character or digit."
        //        };
        //    }

        //    var tokens = await this.ExchangeApplicantReferralCodeForJwt(requestValues);

        //    if (tokens == null || tokens.Success == false)
        //    {
        //        this.logger.LogInformation("An applicant failed to authenticate request {RequestValues} token: {Token}", requestValues.UniqueReferralCode, tokens.AccessToken);
        //        return new AuthResponseDto
        //        {
        //            Success = false,
        //            Error = "Invalid referral code."
        //        };
        //    }

        //    return new AuthResponseDto
        //    {
        //        Success = true,
        //        Data = tokens
        //    };
        //}

        [HttpPost("token-refresh")]
        public async Task<AuthResponseDto> TokenRefresh([FromBody] RefreshTokenModel refreshToken)
        {
            if (refreshToken.RefreshToken == null)
            {
                return new AuthResponseDto { Success = false };
            }

            if (!this.jwtService.ValidateRefreshToken(refreshToken, out var user))
            {
                return new AuthResponseDto { Success = false };
            }

            JwtAuthResponseDto jwts;
            jwts = this.jwtService.GenerateTokensForUser(new JwtUserModel { Id = user.Id});
            return new AuthResponseDto { Success = true, Data = jwts };
        }
    }
}