﻿using Application.Models.Recipe;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecipeApi.Models
{
    internal class SearchRecipesResponseModel
    {
        public List<RecipeSummaryModel> Results { get; set; }
        public string BaseUri { get; set; }
        public int Offset { get; set; }
        public int Number { get; set; }
        public int TotalResults { get; set; }
        public int ProcessingTime { get; set; }
        public long Expires { get; set; }
        public bool IsStale { get; set; }
    }
}
