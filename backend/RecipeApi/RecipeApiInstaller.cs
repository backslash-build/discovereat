﻿using Application.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using RecipeApi.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecipeApi
{
    public static class RecipeApiInstaller
    {

        public static void AddRecipeApiService(this IServiceCollection services)
        {
            services.AddScoped<IRecipeApiService, RecipeApiService>();
        }
    }
}
