﻿using Application.Interfaces;
using Application.Models.Recipe;
using Application.Models.Response;
using Newtonsoft.Json;
using RecipeApi.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace RecipeApi.Services
{
    internal class RecipeApiService : IRecipeApiService
    {
        public async Task<ResponseModel> GetRecipeAsync(long recipeId)
        {
            using (var client = new HttpClient())
            {
                client.AddApiHeaders();

                using (var response = await client.GetAsync("https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com" + "/recipes/" + recipeId + "/information?includeNutrition=true"))
                {
                    var message = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode)
                    {
                        var result = JsonConvert.DeserializeObject<RecipeInformationModel>(message);

                        return new ResponseModel { Data = result, Success = true };
                    }
                    else
                    {
                        var result = JsonConvert.DeserializeObject<dynamic>(message);

                        return new ResponseModel { Success = false, Error = result.message };
                    }
                }
            }
        }

        public async Task<ResponseModel> SearchRecipesAsync(SearchRecipesQueryModel searchRecipesQuery)
        {
            using (var client = new HttpClient())
            {
                client.AddApiHeaders();

                NameValueCollection queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);

                _AddToQueryString(queryString, "query", searchRecipesQuery.Query, null);
                _AddToQueryString(queryString, "cuisine", searchRecipesQuery.Cuisine, null);
                _AddToQueryString(queryString, "diet", searchRecipesQuery.Diet, null);
                _AddToQueryString(queryString, "excludedIngredients", searchRecipesQuery.ExcludedIngredients, null);
                _AddToQueryString(queryString, "intolerances", searchRecipesQuery.Intolerances, null);
                _AddToQueryString(queryString, "number", searchRecipesQuery.Take, "20");
                _AddToQueryString(queryString, "offset", searchRecipesQuery.Skip, "0");
                _AddToQueryString(queryString, "type", searchRecipesQuery.Type, null);
                _AddToQueryString(queryString, "limitLicense", searchRecipesQuery.LimitiLicense, null);
                _AddToQueryString(queryString, "instructionsRequired", searchRecipesQuery.InstructionsRequired, null);

                var uri = queryString.ToString();

                using (var response = await client.GetAsync("https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com" + "/recipes/search?" + uri))
                {
                    var message = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode)
                    {
                        var result = JsonConvert.DeserializeObject<SearchRecipesResponseModel>(message);

                        return new ResponseModel { Data = result, Success = true };
                    }
                    else
                    {
                        var result = JsonConvert.DeserializeObject<dynamic>(message);

                        return new ResponseModel { Success = false, Error = result.message };
                    }
                }
            }
        }

        private NameValueCollection _AddToQueryString(NameValueCollection queryString, string name, string value, string defaultValue)
        {
            value = value ?? defaultValue;

            if (value != null)
            {
                queryString[name] = value;
            }
            return queryString;
        }
    }

    internal static class HttpClientExtensions
    {
        public static HttpClient AddApiHeaders(this HttpClient client)
        {
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("X-RapidAPI-Key", "44e8e074demsh881d7c0e848c2fep1b6478jsn925db90f0ea9");
            client.DefaultRequestHeaders.Add("X-RapidAPI-Host", "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com");
            return client;
        }
    };
}
